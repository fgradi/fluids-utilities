#!/usr/bin/env python
"""
my matplotlib plotting style and some helper functions

Put this into ~/lib/python and
export PYTHONPATH=$PYTHONPATH:$HOME/lib/python

then in your plotting script just
import pub
"""

import numpy as np
import sys

#from matplotlib import rc_file
#rc_file('/home/albrecht/.matplotlib/pub.rc')  # <-- the file containing your settings

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib import cm
from matplotlib import rcParams
import os, errno
import math

gp_palette = ['k', 'r', 'g', 'b', '0.5', 'aqua', 'magenta', 'y', 'sandybrown', 'sienna']
gp_palette += ['0.75'] * 10
symbols = ".+x*s<>^v12348phHDd"

_current_color = -1
_current_symbol = -1

def yield_color():
    global gp_palette
    global _current_color
    _current_color += 1
    if _current_color >= len(gp_palette):
        _current_color = 0
    return gp_palette[_current_color]

def yield_symbol():
    global symbols
    global _current_symbol
    _current_symbol += 1
    if _current_symbol >= len(symbols):
        _current_symbol = 0
    return symbols[_current_symbol]

def m_color(m):
    """Return standard color for azim wavenumber"""
    return gp_palette[m] if m < 10 else '0.75'

def SetPlotRC2():
    #If fonttype = 1 doesn't work with LaTeX, try fonttype 42.
    plt.rc('pdf',fonttype = 42)
    plt.rc('ps',fonttype = 42)
    plt.rc('font', family='serif')


def SetPlotRC():
#    rcParams.keys()
#    print rcParams.keys()
    font_size = 20.7
    rcParams['font.size'] = font_size
    rcParams['axes.labelsize'] = font_size
    rcParams['axes.titlesize'] = font_size
    rcParams['xtick.labelsize'] = font_size
    rcParams['ytick.labelsize'] = font_size
    rcParams['legend.fontsize'] = font_size
    #rcParams['font.family'] = 'serif'
#    rcParams['font.family'] = 'cursive'
#    rcParams['font.family'] = 'sans-serif'
    rcParams['font.serif'] = ['Times New Roman']
    #rcParams['font.serif'] = ['Bookman']
#    rcParams['font.sans-serif'] = ['Computer Modern Sans serif Italic']
    #mpl.rc('font', family = 'serif', serif = 'cmr10')

    rcParams['text.usetex'] = True
    rcParams['text.latex.preamble'] = r'\usepackage{mathrsfs}\usepackage{amsmath}\usepackage{bm}'
    #rcParams['figure.figsize'] = 7.3, 4.2
    rcParams['legend.numpoints'] = 1
    from distutils.version import LooseVersion
    if LooseVersion(matplotlib.__version__) > LooseVersion("1.0"):
        rcParams['legend.frameon'] = False

def BigSymbols():
    rcParams['lines.markersize'] = 8

def ApplyFont(ax):

    print "ApplyFont() deprecated."
    sys.exit(-1)

    ticks = ax.get_xticklabels() + ax.get_yticklabels()

    # -- produces matching font size when includegraphics with 66% tw
    text_size = 20.7
 #   font = "Liberation Sans"
#    font = "Bitstream Vera Serif"
    font = "Times New Roman"
    for t in ticks:
        t.set_fontname(font)
        t.set_fontsize(text_size)

    txt = ax.get_xlabel()
    txt_obj = ax.set_xlabel(txt)
    txt_obj.set_fontname(font)
    txt_obj.set_fontsize(text_size)

    txt = ax.get_ylabel()
    txt_obj = ax.set_ylabel(txt)
    txt_obj.set_fontname(font)
    txt_obj.set_fontsize(text_size)

    txt = ax.get_title()
    txt_obj = ax.set_title(txt)
    txt_obj.set_fontname(font)
    txt_obj.set_fontsize(text_size)


    legend = ax.get_legend()
    if legend:
        texts = legend.get_texts()
        for txt_obj in texts:
            txt_obj.set_fontname(font)
            txt_obj.set_fontsize(text_size)


def unset_xtics():
    plt.axes().get_xaxis().set_ticklabels([])
def unset_ytics():
    plt.axes().get_yaxis().set_ticklabels([])

def xtics(major_freq):
    plt.axes().xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(major_freq))

def mxtics(minor_freq):
    plt.axes().xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(minor_freq))

def ytics(major_freq):
    plt.axes().yaxis.set_major_locator(matplotlib.ticker.MultipleLocator(major_freq))

def mytics(minor_freq):
    plt.axes().yaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(minor_freq))

def unset_outer_spines(ax=None):
    if ax is None:
        ax = plt.gca()
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')

def loadtxt(filename, usecols):
    f = open(filename, 'r')
    data = []
    is_float = np.ones(len(usecols))
    for col in usecols:
        data.append([])
    for line in f.readlines():
        if line.startswith('#') or line.strip() == "": continue
        splitted = line.split()
        for i, col in enumerate(usecols):
            value = splitted[col]
            try:
                value = float(value)
            except ValueError:
                is_float[col] = 0
            data[i].append(value)
    f.close()

    new_data = []
    for i, the_data_col in enumerate(data):
        if is_float[i]:
            new_data.append(np.array(the_data_col))
        else:
            new_data.append(the_data_col)

    return new_data

def autolev(z, N, filled=False, logscale=False):
    """
    Select contour levels to span the data.

    We need two more levels for filled contours than for
    line contours, because for the latter we need to specify
    the lower and upper boundary of each range. For example,
    a single contour boundary, say at z = 0, requires only
    one contour line, but two filled regions, and therefore
    three levels to provide boundaries for both regions.
    """
    if logscale:
        locator = ticker.LogLocator()
    else:
        locator = ticker.MaxNLocator(N + 1)
    zmax = z.max()
    zmin = z.min()
    lev = locator.tick_values(zmin, zmax)
    if filled:
        return lev
    # For line contours, drop levels outside the data range.
    return lev[(lev > zmin) & (lev < zmax)]

def shiftedColorMap(cmap, start=0, midpoint=0.5, stop=1.0, name='shiftedcmap'):
    '''
    Function to offset the "center" of a colormap. Useful for
    data with a negative min and positive max and you want the
    middle of the colormap's dynamic range to be at zero

    Input
    -----
      cmap : The matplotlib colormap to be altered
      start : Offset from lowest point in the colormap's range.
          Defaults to 0.0 (no lower ofset). Should be between
          0.0 and `midpoint`.
      midpoint : The new center of the colormap. Defaults to
          0.5 (no shift). Should be between 0.0 and 1.0. In
          general, this should be  1 - vmax/(vmax + abs(vmin))
          For example if your data range from -15.0 to +5.0 and
          you want the center of the colormap at 0.0, `midpoint`
          should be set to  1 - 5/(5 + 15)) or 0.75
      stop : Offset from highets point in the colormap's range.
          Defaults to 1.0 (no upper ofset). Should be between
          `midpoint` and 1.0.
    '''
    cdict = {
        'red': [],
        'green': [],
        'blue': [],
        'alpha': []
    }

    # regular index to compute the colors
    reg_index = np.linspace(start, stop, 257)

    # shifted index to match the data
    shift_index = np.hstack([
        np.linspace(0.0, midpoint, 128, endpoint=False),
        np.linspace(midpoint, 1.0, 129, endpoint=True)
    ])

    for ri, si in zip(reg_index, shift_index):
        r, g, b, a = cmap(ri)

        cdict['red'].append((si, r, r))
        cdict['green'].append((si, g, g))
        cdict['blue'].append((si, b, b))
        cdict['alpha'].append((si, a, a))

    newcmap = matplotlib.colors.LinearSegmentedColormap(name, cdict)
    plt.register_cmap(cmap=newcmap)

    return newcmap

def shifted_color_map(levels, cmap=cm.RdBu_r):
    """return a colormap where the zero level is white and the shorter end is cut"""

    assert(levels.max() >= 0)
    assert(levels.min() <= 0)
    if abs(levels.min()) > levels.max():
        start = 0.
        stop =  0.5 + 0.5 * levels.max() / abs(levels.min())
    else:
        stop = 1.
        start = 0.5 - 0.5 * abs(levels.min()) / levels.max()

    return shiftedColorMap(cmap, start=start, midpoint=0.5, stop=stop, name='shrunk')


GEOM_LAPTOP_FULLSCREEN = (0, 10, 1440, 850)
GEOM_LAPTOP_RIGHT_HALF = (720, 10, 720, 850)
GEOM_LAPTOP_RIGHT_UPPER = (720, 10, 720, 425)
GEOM_LAPTOP_LEFT_HALF  = (0, 10, 720, 850)
GEOM_MONASH_UPPER_LEFT = (0, 10, 1120, 750)
GEOM_MONASH_UPPER_LEFT_SMALL = (0, 10, 1120/2, 750/2)
GEOM_MONASH_LEFT_HALF  = (0, 10, 1120, 1200)

def set_window(geometry):
    thismanager = plt.get_current_fig_manager()
    thismanager.window.setGeometry(*geometry)


def mkdirs(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

#import os
import fnmatch
def sub_glob(path, pattern):
    """recursively find files matching pattern"""
    return [os.path.join(dirpath, f)
        for dirpath, dirnames, files in os.walk(path)
        for f in fnmatch.filter(files, pattern)]

def frexp_10(decimal):
   parts = ("%e" % decimal).split('e')
   return float(parts[0]), int(parts[1])

def decimal_to_sci_latex_string(decimal):
    """produce a latex-formatted number in sci format from given decimal"""
    if decimal == 1.0:
        return ""
    if decimal == 10.:
        return "/ 10"

    mant, expo = frexp_10(decimal)
    if mant == 1.0:
        return "/ 10^{%i}" % expo
    else:
        return "/ %g \times 10^{%i}" % (mant, expo)

if __name__ == "__main__":

    SetPlotRC()
    levels = np.linspace(-2., 2, 11)

    t = np.arange(0, 2*np.pi, 0.01)
    y = np.sin(t)

    plt.plot(t,y)
    plt.xlabel("Time $ReRo$")
    plt.ylabel("Signal")
    plt.title("Sine Wave")
    plt.text(1,0.5, 'Sine Waveb 1.297')
    #ax = plt.gca()

    #ApplyFont(plt.gca())
    plt.savefig("sine.eps")

else:
    #SetPlotRC()
    pass