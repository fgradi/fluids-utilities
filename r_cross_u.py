#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#

"""
FIXME: do we still have a bug here? 
Compute total angular momentum: R cross U, project to Cartesian, integrate over domain. Print t,u,v,w
"""

import numpy as np
import sys
from pdb import pm
import argparse
import fieldfile
import semesh
import copy
import subprocess    


if __name__ == "__main__":
    """
    """
    
    parser = argparse.ArgumentParser(description="Compute total angular momentum: R cross U, project to Cartesian, integrate over domain. Print t,u,v,w")
    parser.add_argument("-m", "--mesh", type=str, default=None, help="mesh file")
    parser.add_argument('fieldfile', metavar='field.chk', type=str, nargs='+',
                   help='the field file')
    args = parser.parse_args()

    field_name = args.fieldfile[0]
    
    if args.mesh is None:
        session = field_name.split('.')[0]
        mesh = semesh.Semesh(session + '.mesh')
    else:
        session = args.mesh.split('.')[0]
        mesh = semesh.Semesh(args.mesh)
    mesh.to_3d()
    
    R = np.array([mesh.X, mesh.Y, np.zeros_like(mesh.X)])
    
    for the_file in args.fieldfile:
        ff = fieldfile.Fieldfile(the_file, "r", read_data=True)
        U = np.array([ff.data_dict['u'], ff.data_dict['v'], ff.data_dict['w']])
        r_cross_u = np.cross(R, U, axis=0)

        print r_cross_u[0].mean()
        # -- project to Cartesian
        print "does the mesh have Z?"
        print mesh.Z
        r_cross_u = fieldfile.components_cyl_to_cart(r_cross_u, mesh)
    
        # -- write output
        proc = subprocess.Popen(['integral', session], stdin=subprocess.PIPE, stdout=subprocess.PIPE)

        hdr = copy.copy(ff.hdr)
        hdr.fields = 'uvw'
        ff_out = fieldfile.Fieldfile(proc.stdin, 'w', hdr)

        ff_out.write_fields(r_cross_u)
    
        result = proc.stdout.readlines()
        u, v, w = [float(line.split(',')[0][2:]) for line in result[1:]]
        print ff.hdr.time, u, v, w
    