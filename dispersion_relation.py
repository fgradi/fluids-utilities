#!/usr/bin/env python
"""Compute the dispersion relation for Kelvin modes.

For a given azimuthal wave number m, it relates the
axial wave number k to a (forcing) angular frequency w:

  w C J_m'(C) + 2J_m(C) = 0     (Eq. (3) in Lagrange et al PoF 2008)

with C = k * sqrt(4/w^2 - 1)

J_m()  is the Bessel function of first kind,
J_m'() its first derivative, which can be expressed by the recurrance relation
       J_m'(z) = J_(m-1)(z) - m/z J_m(z)

latexit.sh '\delta_i^2 = \frac{4-\omega^2}{\omega^2}k^2'
latexit.sh '\omega \delta_i J_1\'(\delta_i)'


- pick m
- start with small w and k
- increase k by small steps dk until sign of (3) changes
- find root of (3) within that inverval -> first point of branch
- follow that branch:
  - w += dw_small
  - find k
  - while k < kmax:
    - w += dw_large
    - predict/correct next k
- compute other branches by starting at the second, third, ... root

IRPHE notation
--------------
W1 = cylinder rotation
W2 = table rotation
tilt angle alpha
W = W1 + W2 * cos(alpha)

freq ratio  w = W1 / W = W1 / (W1 + W2 * cos(alpha))
Re            = W R^2/nu
h = H/R

Kelvin modes
------------
h=1.62
w=1.18

- freq rotion w -> k
- if any of k fits inside cylinder -> it becomes resonant
  k = (2n+1)*pi/h

(c) 2014-2017 Thomas Albrecht
"""

from scipy.special import jn
import scipy.optimize as spo
import matplotlib.pyplot as plt
import numpy as np
import math
from pdb import pm
import sys
import pub
import logging
verbose = 0
has_been_warned = False


def f_long(k, w, m):
    """The dispersion relation -- long version. PROBABLY MISSING M"""
    raise ValueError("bug: probably missing m in here!")
    return w * k * (4/(w*w)-1)**0.5 \
      * (jn(m-1, k*(4/(w*w)-1)**0.5) - m/(k*(4/(w*w)-1)**0.5) * jn(m, k*(4/(w*w)-1)**0.5) ) \
      + 2 * jn(m, k*(4/(w*w)-1)**0.5)


def jnp(m,x):
    """Return J_m'(), the derivative of J_m().

    J_m()  is the Bessel function of first kind,
    J_m'() its first derivative, which can be expressed by the recurrance relation
           J_m'(z) = J_(m-1)(z) - m/z J_m(z)
    """
    return jn(m-1,x) - m/x * jn(m,x)


def f(k, w, m):
    """The dispersion relation"""
    di = (4. /(w * w) - 1)**0.5 * k
    return w * di * jnp(m, di) + 2*m*jn(m, di)


def f_(w, k, m):
    return f(k, w, m)


def di(k, w):
    return (4/w**2-1)**0.5 * k


def sign(a):
    return math.copysign(1, a)


def absmin(a, b):
    if abs(a) < abs(b): return a
    else: return b


def find_first_roots(n, k0, dk, w0, dw, m):
    """If dw is nonzero, find first roots in w at given k0, m.
       if dw is zero,    find first roots in k at given w0, m.
    """
    initial_dw = dw
    initial_dk = dk
    roots = []
    for i in range(n):
        tmp, k, tmp, w, sign1 = find_sign_change(1, k0, dk, w0, dw, m)
        # ensure our steps are smaller than 1/5 of the distance to first sign change
        dk = absmin((k - k0)/5., initial_dk)
        dw = absmin((w - w0)/5., initial_dw)
        if dw == 0:
            if verbose: print "\n# looking in k = [%g, %g]" % (k0, k),
            k = spo.brentq(f, k0, k, args=(w0, m), full_output=False)
            roots.append(k)
            k0 = k + dk
            if verbose: print k
        else:
            if verbose: print "\n# looking in w = [%g, %g]" % (w0, w),
            w, result = spo.brentq(f_, w0, w, args=(k0, m), full_output=True)
            roots.append(w)
            w0 = w + dw
            if verbose: print w

    return roots


def find_first_roots_at_k(n, k, w0, dw, m):
    return find_first_roots(n, k, 0, w0, dw, m)


def find_first_roots_at_w(n, k0, dk, w, m):
    return find_first_roots(n, k0, dk, w, 0, m)


def find_sign_change(i, k0, dk, w0, dw, m):
    """Start at (k0, w0). Step (dk, dw) and register sign changes along the way.
       Stop at the i'th sign change."""
    assert(i > 0)
    sgn0 = sign(f(k0, w0, m))
    k = k0
    w = w0
    while True:
        k = k0 + dk
        w = w0 + dw
        sgn = sign(f(k, w, m))
        if verbose: print "sign @ %g %g was %g, now @ %g %g is %g" % (k0, w0, sgn0, k, w, sgn)
        if sgn != sgn0:
            i -= 1
            if i < 1:
                return k0, k, w0, w, sgn
        sgn0 = sgn
        k0 = k
        w0 = w


def warn_once(msg):
    global has_been_warned
    if not has_been_warned:
        sys.stderr.write(msg)
        has_been_warned = True


def k_resonant(h, n):
    """Return n'th (starting at one) resonant axial wavenumber k for given h"""
    assert (n >= 1)
    warn_once("\n---------------------\nCHECK YOUR n! k_resonant() has changed.\n\n")
    return n * np.pi / h


def get_delta_k(k, h):
    """given k, return minimum distance to an exact resonance for a given h"""
    step = np.pi / h
    k_exact = np.round(k / step) * (step)
    return k_exact - k


def plot_surface():
    k = np.linspace(0,4)
    w = np.linspace(0,1)
    fs = np.zeros((len(k), len(w)))
    for i, k in enumerate(k):
        for j, wi in enumerate(w):
            fs[i,j] = f(k, wi, 1)
            print "%g %g %g" % (k, wi, fs[i,j])


def get_branch(l, m, kmax, negative=False, dw_step=0.01):
    """get one branch of the dispersion relation. Branch l starts with 1.
       Returns K, W
    """
    if negative:
        w = -0.01
        dw_small = -0.0001
        dw_large = -dw_step
    else:
        w = 0.01
        dw_small = 0.0001
        dw_large = dw_step
    k0 = 1e-5
    dk = 0.01

    k0, k, tmp1, tmp2, sign1 = find_sign_change(l, k0, dk, w, 0, m)
    if verbose: print "# sign change at", k,

    # -- find root as starting point
    if verbose: print "\n# looking in [%g, %g]" % (k0, k),
    k = spo.brentq(f, k0, k, args=(w, m))
    if verbose: print "k = ", k
    W = [w]
    K = [k]
    K_guess = [k]

    w0 = w
    k0 = k
    w += dw_small
    k = spo.fsolve(f, k, args=(w, m))[0]

    while k < kmax:
        dkdw = (k - k0)/(w - w0)
        k0 = k
        w0 = w
        w += dw_large
        k_guess = k0 + dkdw * dw_large
        k = spo.fsolve(f, k_guess, args=(w, m))[0]
#        print "w, k, guess =", w, k, k_guess
        W.append(w)
        K.append(k)
        K_guess.append(k_guess)

    return np.array(K), np.array(W)


def rm92_to_irphe(wp=None, hp=None):
    out = []
    if wp: out.append(2./wp)
    if hp: out.append(hp)

    if len(out) == 1: return out[0]
    else: return out


def richard_line(H, n, l, m):

    D = 90.
    R = D/2.
    h = H/R
    k = k_resonant(h, n)
    Ws = find_first_roots_at_k(l, k, 1.9999, -0.0001, m)

    w = Ws[l-1]
    d = di(k, w)
    print "%3.0f %i %6.3f %i %i --- %5.3f %5.3f %5.3f %5.3f" % \
      (H, n, k, l, m, d, H/D, w, h)
    return k, w


def add_RM_points():
    """from sheet"""
    p = np.array([ [90, 1, .996],
                  [178, 2, 1.005],
                   [89, 3, 1.778],
                   [100,3, 1.729],
                   [200,3, 1.259],
                   [89, 1, 0.343],
                   [82, 3, 1.017],
                   [86, 2, 1.] ])

    for (H, n, w) in p:
        D = 90.
        R = D/2.
        h = H/R
        k = n * np.pi/h
        print "k", k, "n", n, H
        plt.plot(k, w, 'gx', markersize=12)

def add_RM_file():
    m = np.loadtxt('n116004.plt')
    plt.plot(m[:,0], m[:,3], 'g.')


def compare_with_richard():

    def line(H, n, l, m):
        sym = ['ro', 'ro', 'ro']
        k, w = richard_line(H, n, l, m)
        plt.plot(k, w, sym[l-1])

    print "# h n  k     l m xxx lam   h/D   w     h"
    line(89,1,1,1)
    line(90,1,1,1)
    line(91,1,1,1)
    print
    line(178,2,1,1)
    print
    line(89,3,1,1)
    line(100,3,1,1)
    line(200,3,1,1)
    print
    line(89,1,2,1)
    line(89,1.1,1,1)
    line(89,1,3,1)
    print
    line(120,3,1,1)

    plt.savefig('disp_rel_TA_RM.pdf')
    plt.show()


def get_Om1_Om2(Re, om, theta_rad):
    """compute Omega1 and Omega2 in dimensional units from Re, omega, and theta"""
    logging.warn("DEPRECATED CALL of dr.get_Om1_Om2(). Use dr.get_omegas().")
    return get_Omegas(Re, om, theta_rad)[0:2]


def get_Omegas(Re, om, theta_rad, nu = 1e-6):
    """compute all Omegas in dimensional units from Re, omega, and theta"""
    R = 92.3/1000.
    Om  = Re * nu / R**2
    Om1 = om * Om
    Om2 = (Om - Om1) / np.cos(theta_rad)
    return Om, Om1, Om2


def get_om(k, n=1, l=1, m=1):
    """Fixed: axial and radial int wave numbers are 1-based
       n can be negative
    """
    D = 92.3/1000.
    R = D/2.

    if n < 0:
        sgn = -1.
        n = -n
    else:
        sgn = 1.

    # -- first n_res resonances
    Ws = np.array(find_first_roots_at_k(l, k, 1.9999*sgn, -0.0001*sgn, m))

    return Ws[l-1]


def get_Ro(omega, theta_deg):
    return (1-omega)*np.tan(np.deg2rad(theta_deg))


def get_Po(omega, theta_deg):
    return (1./omega - 1) / np.cos(np.deg2rad(theta_deg))


def plot_any_DR(h, m1, theta_deg, Po=None):
    """WIP"""
    global verbose
    import pub
    pub.SetPlotRC()
    m2 = m1 + 1
    if not Po:
        omega, k = get_om_k(h, 1, 1, 1)
    else:
        omega = 1./(Po*np.cos(theta_deg)+1)

    verbose = 0
    steps_per_resonance = 1
    K = np.linspace(0, 6, 6 * steps_per_resonance + 1) * np.pi / h + 0.01
    for l in range(1, 6):
        OM_F = np.zeros_like(K)
        OM_A = np.zeros_like(K)
        OM_An = np.zeros_like(K)
        OM_B = np.zeros_like(K)
        OM_Bn = np.zeros_like(K)
        for i, k in enumerate(K):
            print k, i, l

            OM_A[i] = get_om(k,   1, l, m=m1)
            OM_Bn[i] = get_om(k, -1, l, m=m2)

            if l < 3:
                OM_An[i] = get_om(k, -1, l, m=m1)
                OM_B[i] = get_om(k,   1, l, m=m2)

        # -- arrows
        if 0 and l == 1:
            ax = plt.axes()
            a1_om = OM_An[1 * steps_per_resonance] - 5
            a1_k = K[1 * steps_per_resonance]
            # should be 27 x 15, is 20 x 20
            hl = 0.3*27/20.
            hw = 0.15*15/20.
            ax.arrow(0, 0, a1_k, a1_om, head_width=hw, head_length=hl, length_includes_head=True, fc='k', ec='k', zorder=0)
            plt.text(0.4, -4, '$(k_5, \omega_5)$', backgroundcolor='w')

            a2_om = OM_B[2 * steps_per_resonance] - 6
            a2_k = K[2 * steps_per_resonance]
            ax.arrow(0, 0, a2_k, a2_om, head_width=hw, head_length=hl, length_includes_head=True, fc='k', ec='k', zorder=0)
            plt.text(2.1, -3.9, '$(k_6, \omega_6)$', backgroundcolor='w')

            ax.arrow(a1_k, a1_om, (a2_k - a1_k), (a2_om - a1_om), head_width=0.15, head_length=0.3, length_includes_head=True, fc='k', ec='k', zorder=0)
            plt.text(a1_k+.23, a1_om + 0.12, '$(k_1, 0)$')

        if 0 and l < 3:
            plt.plot(K, OM_An - 5, 'k--')
            plt.plot(K, OM_B  - 6, 'k-')

        plt.plot(K, OM_A - 5, 'k--')
        plt.plot(K, OM_Bn - 6, 'k-')

    # -- resonance lines and labels
    for n in range(1, 6):
        the_k = n * np.pi / h
        if n > 1:
            plt.text(the_k-0.08, -1.37, n, backgroundcolor='w')
        else:
            plt.text(the_k-0.6, -1.37, "$n=1$", backgroundcolor='w')
        plt.plot([the_k, the_k], [-7, 0], 'k-.')

    plt.ylim(-7, 0)
    plt.xlim(0, 10)
    plt.ylabel('$\omega$')
    plt.xlabel('$k$')
    plt.savefig('dispersion_relation_cylinder_FoR.eps', transparent=True, bbox_inches='tight')


def plot_DR():
    """The DR for our 2015 JFM"""
    global verbose
    import pub
    pub.SetPlotRC()
    h = 1.62
    verbose = 0
    steps_per_resonance = 1
    K = np.linspace(0, 6, 6 * steps_per_resonance + 1) * np.pi / h + 0.01
    for l in range(1, 6):
        W1 = np.zeros_like(K)
        W5 = np.zeros_like(K)
        W5n = np.zeros_like(K)
        W6 = np.zeros_like(K)
        W6n = np.zeros_like(K)
        w1 = 1.18
        for i, k in enumerate(K):
            print k, i, l

            W5[i] = get_om(k,   1, l, m=5) / w1
            W6n[i] = get_om(k, -1, l, m=6) / w1

            if l < 3:
                W5n[i] = get_om(k, -1, l, m=5) / w1
                W6[i] = get_om(k,   1, l, m=6) / w1

        # -- arrows
        if l == 1:
            ax = plt.axes()
            a1_om = W5n[1 * steps_per_resonance] - 5
            a1_k = K[1 * steps_per_resonance]
            # should be 27 x 15, is 20 x 20
            hl = 0.3*27/20.
            hw = 0.15*15/20.
            ax.arrow(0, 0, a1_k, a1_om, head_width=hw, head_length=hl, length_includes_head=True, fc='k', ec='k', zorder=0)
            plt.text(0.4, -4, '$(k_5, \omega_5)$', backgroundcolor='w')

            a2_om = W6[2 * steps_per_resonance] - 6
            a2_k = K[2 * steps_per_resonance]
            ax.arrow(0, 0, a2_k, a2_om, head_width=hw, head_length=hl, length_includes_head=True, fc='k', ec='k', zorder=0)
            plt.text(2.1, -3.9, '$(k_6, \omega_6)$', backgroundcolor='w')

            ax.arrow(a1_k, a1_om, (a2_k - a1_k), (a2_om - a1_om), head_width=0.15, head_length=0.3, length_includes_head=True, fc='k', ec='k', zorder=0)
            plt.text(a1_k+.23, a1_om + 0.12, '$(k_1, 0)$')

        if l < 3:
            plt.plot(K, W5n - 5, 'k--')
            plt.plot(K, W6  - 6, 'k-')

        plt.plot(K, W5 - 5, 'k--')
        plt.plot(K, W6n - 6, 'k-')

    # -- resonance lines and labels
    for n in range(1, 6):
        the_k = n * np.pi / h
        if n > 1:
            plt.text(the_k-0.08, -1.37, n, backgroundcolor='w')
        else:
            plt.text(the_k-0.6, -1.37, "$n=1$", backgroundcolor='w')
        plt.plot([the_k, the_k], [-7, 0], 'k-.')

    plt.ylim(-7, 0)
    plt.xlim(0, 10)
    plt.ylabel('$\omega$')
    plt.xlabel('$k$')
    plt.savefig('dispersion_relation_table_FoR.eps', transparent=True, bbox_inches='tight')
    #plt.show()

def get_om_k(h, n=1, l=1, m=1):
    """Fixed: axial and radial int wave numbers are 1-based
       n can be negative
    """

    if n < 0:
        sgn = -1.
        n = -n
    else:
        sgn = 1.

    # -- first n_res resonances
    k = k_resonant(h, n)
    Ws = np.array(find_first_roots_at_k(l, k, 1.9999*sgn, -0.0001*sgn, m))

        # -- find w for given resonant k, m
    #Ws = np.array(find_first_roots_at_k(n_roots, k, 1.9999, -0.001, m))
    om = Ws[l-1]

    return om, k


def schedule_line(theta_deg, Re, h, res = [1], ls = [1], head=False):
    """Fixed: axial and radial int wave numbers are 1-based"""
    D = 92.3/1000.
    R = D/2.

    n_roots = 2 # radial modes?

    theta = np.deg2rad(theta_deg)
    nu = 1e-6

    m = 1

    # -- first n_res resonances
    for n in res:
        k = k_resonant(h, n)
        # -- find w for given resonant k, m
        Ws = np.array(find_first_roots_at_k(n_roots, k, 1.9999, -0.001, m))
        Ws = Ws[np.array(ls)-1]
        OK = 0
        for il, w in enumerate(Ws):
            if 1:
                try:
                    l = ls[il]
                except IndexError:
                    l = -1
                    n = -1
            d = di(k, w)
            W1 = w * nu * Re / R**2
            W2 = (nu * Re / R**2 - W1) / np.cos(theta)
            W = W1 + W2 * np.cos(theta)
            W2rpm = W2/(np.pi*2)*60.
            Re_check = W * R**2 / nu
            remark = ""
            if head:
                print "h     the  Re     n l w      delta  W1      W2      (-rpm) g "
                head = False
            Rmax = (0.4**2+0.5**2)**0.5
            g = W2**2 * Rmax / 9.81
            remark = 'good'
            if W1 > 50 or W1 < 0.18 or abs(W2rpm) > 15. or g > 2.:
                remark = "OOR"
            else:
                OK += 1

            Ro = W2 * np.sin(theta) / W

            z = 0.
            filename = "h%1.3f_t%04.1f_R%04g_k%i_l%i_m%+07.3f_r%+06.2f_z" % (h, theta_deg, Re_check/100., n+1, l+1, W1, -W2rpm)
            filename = filename.replace('.',',')
            print "%1.3f %4.1f %6g %i %i %1.3f %6.3f % 7.3f % 7.3f % 7.2f %4.1f %3i %4s %s %1.3f" % \
              (h, theta_deg, Re_check, n, l, w, d, W1, W2, -W2rpm, g, z, remark, filename, abs(Ro*Re))

    return OK

def schedule_line_W2(theta_deg, W2, h, res = [1,2], ls = [1,2], head=False):
    """Fixed: axial and radial int wave numbers are 1-based"""
    D = 92.3/1000.
    R = D/2.

    n_roots = 2 # radial modes?

    theta = np.deg2rad(theta_deg)
    nu = 1e-6
    m = 1

    # -- first n_res resonances
    for n in res:
        k = k_resonant(h, n)
        # -- find w for given resonant k, m
        Ws = np.array(find_first_roots_at_k(n_roots, k, 1.9999, -0.001, m))
        Ws = Ws[np.array(ls)-1]
        OK = 0
        for il, w in enumerate(Ws):
            if 1:
                try:
                    l = ls[il]
                except IndexError:
                    l = -1
                    n = -1
            d = di(k, w)
            #W1 = w * nu * Re / R**2
            #W2 = (nu * Re / R**2 - W1) / np.cos(theta)
            W1 = np.cos(theta)*W2/(1./w-1.)
            if W1 < 0:
                W2 = -W2
                W1 = np.cos(theta)*W2/(1./w-1.)

            W2rpm = W2/(np.pi*2)*60.
            W = W1 + W2 * np.cos(theta)
            Re_check = abs(W) * R**2 / nu
            remark = ""
            if head:
                print "h     the  Re     n l w      delta  W1      actual  W2      (-rpm)  d_t    g "
                head = False
            Rmax = (0.4**2+0.5**2)**0.5
            #print Rmax
            g = W2**2 * Rmax / 9.81
            remark = 'good'
            if 0 and (W1 > 50 or W1 < 0.18 or abs(W2rpm) > 15. or g > 2.):
                remark = "OOR"
            else:
                OK += 1

            z = 999
            delta_t = 0.1/W1
            if delta_t > 0.21: delta_t = 0.21  # -- PIV delta_t cannot be larger than 210 msec.
            f = 1.

            filename = "h%1.3f_t%04.1f_R%06.0f_k%i_l%i_m%+07.3f_r%+06.2f_z%03i_d%03.0f_f%4.2f" % \
                       (h, theta_deg, Re_check, n, l, W1, -W2rpm, z, delta_t*1000, f)
            filename = filename.replace('.',',')
            print "%1.3f %4.1f %6.0f %i %i %1.3f %6.3f % 7.3f % 7.3f % 7.3f % 7.2f %6.3f %4.1f %3i %s %4s" % \
              (h, theta_deg, Re_check, n, l, w, d, W1, 0, W2, -W2rpm, delta_t, g, z, filename, remark)
    return OK


def schedule():
    theta_degs = [0.5, 2, 5, 15]
    theta_degs = [45.0]
    #Res = [1400, 14000, 90000]
    #Res = [6500]
    W2s = -np.array([2, 5, 10]) / 60. * (2*np.pi)
    W2s = [0.000002]
    #hs = [0.5, 1., 1.612, 1.8, 2.615, 2.990]
    #hs = [2.615, 1.8, 1., 0.5]
    hs = [1.835]
    hs = [1.0]
    hs = [1.98981612109225]
    #hs = [2.615]
    #hs = [1.620]
    OK = 0
    for h in hs:
        for theta_deg in theta_degs:
            head = True
            for W2 in W2s:
                thisOK = schedule_line_W2(theta_deg, W2, h, res = [1], ls = [1], head=head)
                head = False
                if not thisOK:
                    thisOK = schedule_line_W2(theta_deg, W2, h, res = [2], ls = [1], head=head)
                OK += thisOK
    print "possible %i" % (OK)

# Fig3a schedule_line(1, 5500, 2, res = [0], ls = [0], head=True)
# Fig5a
#schedule_line(1, 11800, 2, res = [0], ls = [1], head=True)

def check_negative_omega():
    """check negative omega with RM96 table 1 data."""
    h = rm92_to_irphe(hp=8/3.)

    def check(n, l, m, wp, negative=True):
        k = k_resonant(h, n)
        if negative:
            Ws = find_first_roots_at_k(l, k, -1.9999, 0.0001, m)
        else:
            Ws = find_first_roots_at_k(l, k,  1.9999, -0.0001, m)

        print "%i %i %i % 5.3f % 5.3f %5.3f %5.3f " % (n, l, m, Ws[-1], rm92_to_irphe(wp), Ws[-1] / rm92_to_irphe(wp), di(k, Ws[-1]))

    check(1, 1, 1, -4.056, True)
    check(1, 2, 1, -6.733, True)
    check(2, 1, 1, -2.305, True)
    check(2, 2, 1, -3.590, True)
    check(3, 1, 1, -1.732, True)
    check(3, 2, 1, -2.537, True)
    print
    check(1, 1, 1, 2.643, False)
    check(1, 2, 1, 5.278, False)
    check(2, 1, 1, 1.484, False)
    check(2, 2, 1, 2.654, False)
    check(3, 1, 1, 1.224, False)
    check(3, 2, 1, 1.891, False)
    print
    check(1, 1, 2, -4.893, True)
    check(1, 2, 2, -7.649, True)
    check(2, 1, 2, -2.712, True)
    check(2, 2, 2, -4.050, True)
    check(3, 1, 2, -1.994, True)
    check(3, 2, 2, -2.846, True)
    print
    check(1, 1, 2, 4.014, False)
    check(1, 2, 2, 6.755, False)
    check(2, 1, 2, 2.056, False)
    check(2, 2, 2, 3.346, False)
    check(3, 1, 2, 1.524, False)
    check(3, 2, 2, 2.304, False)


def get_theta_from_Ro(Ro, omega):
    return np.arctan(Ro/(1. - omega))


def get_intersect((K1, OM1), (K2, OM2)):
    """given two branches, find intersection where OM1(k) == OM2(k). Return k"""
    # -- get splines for both
    import scipy.interpolate
    OM1_interp = scipy.interpolate.interp1d(K1, OM1, kind='cubic')
    OM2_interp = scipy.interpolate.interp1d(K2, OM2, kind='cubic')

    # -- minimize abs(OM2 - OM1)
    def diff(k):
        return OM1_interp(k) - OM2_interp(k)
    # add / subtract eps to avoid out of bounds error, as brentq appears to probe
    # outside given interval
    eps=0.1
    k_intersect = spo.brentq(diff, max(K1[0], K2[0])+eps, min(K1[-1], K2[-1])-eps)
    om_intersect = OM1_interp(k_intersect)
    return k_intersect, om_intersect


def plot_relation(h, m1, theta_deg, m2=None, Po=None, show=True, showm1=True, showm2=True,
                  showlegend=True, lmax=3, filename=None, ylim=(-1,2),
                  shift1_omega=True, shift1_k=True, retrograde=False,
                  show_resonances=True):
    """Validated OK with JFM 2011 Fig 3"""
    global verbose
    import pub
    pub.SetPlotRC()
    """
    8 combinations:
      2: +omega_1 and -omega_2  or vice versa
      2: k = 1,2 or 2,1
      2: omega_F positive or negative (pro- or retrograde)
    """


    if not m2:
        m2 = m1 + 1
#    plt.figure(figsize=(4,3))
    if retrograde:
        m_F = -1
    else:
        m_F = 1

    sign_m1 = 1 if m1 > 0 else -1
    sign_m2 = 1 if m2 > 0 else -1
    m1 = abs(m1)
    m2 = abs(m2)

    omega_F, k_F = get_om_k(h, m_F, 1, 1)
#    omega_F = 2.5
    print "Omega =", omega_F
    if Po:
        print "Omega at Mode 1,1,1 resonance =", omega_F,
        omega_F = 1./(Po*np.cos(np.deg2rad(theta_deg))+1.)
        print "but using", omega_F

    lc = ['c','r','b','b','k','m','y','k','r', 'g'] # line colors

    # forced mode
    col = pub.gp_palette[1]
#    col = lc[0]
    if omega_F <= 2.:
        for i in np.arange(lmax) + 1:
            label = None
            lw = 1
            if i == 1:
                label=r"$m_F = 1$"
                lw = 2
            K, W = get_branch(i, m=1, kmax=10, negative=False)
            plt.plot(K, W, '-', color=col, label=label, linewidth=lw)

            K, W = get_branch(i, m=1, kmax=10, negative=True)
            plt.plot(K, W, '-', color=col, linewidth=lw)


    # free #1  -- this is usually the shifted one
    col = pub.gp_palette[m1]
#    col = lc[1]
    if showm1:
        for i in np.arange(lmax) + 1:
            lw = 1
            label = None
            if i == 1:
                label=r"$m_1 = %i$" % m1
                lw = 2
            # -- draw positive branch
            K, W = get_branch(i, m=m1, kmax=10, negative=False)
            W *= sign_m1 # invert branch if m is negative
            shift_K = k_F * shift1_k
            shift_W = omega_F * shift1_omega
            print "shift m1 by", shift_K, shift_W
            #shift_K = shift_W = 0 # TEST
            plt.plot(K + shift_K, W + shift_W, '-', color=col, label=label, linewidth=lw)

            # -- draw negative branch
            K, W = get_branch(i, m=m1, kmax=10, negative=True)
            W *= sign_m1
            plt.plot(K + shift_K, W + shift_W, '-', color=col, linewidth=lw)


    # free #2
    col = pub.gp_palette[m2]
    if showm2:
        for i in np.arange(lmax) + 1:
            lw = 1
            label = None
            if i == 1:
                lw = 2
                label=r"$m_2 = %i$" % m2
            K, W = get_branch(i, m=m2, kmax=10, negative=False)
            W *= sign_m2
            shift_K = k_F * (not shift1_k)
            shift_W = omega_F * (not shift1_omega)
            plt.plot(K + shift_K, W + shift_W, '-', color=col, label=label, linewidth=lw)

            K, W = get_branch(i, m=m2, kmax=10, negative=True)
            W *= sign_m2
            plt.plot(K + shift_K, W + shift_W, '-', color=col, linewidth=lw)

    y0, y1 = ylim
    plt.ylim(y0, y1)
    x0 = 0
    x1 = 4
    plt.xlim(x0,x1)
    plt.xlabel('$k$')
    plt.ylabel(r'$\omega$')
    if showlegend:
        plt.legend(loc='lower right')

    # -- resonance lines and labels
    if show_resonances:
        top_ofs=0.5
        shown=1
        for n in range(1, 8):
            the_k = n * np.pi / h
            if the_k > x1: continue
            if shown:
                if n > 1:
                    plt.text(the_k-0.08, y1-top_ofs, n, backgroundcolor='w')
                else:
                    plt.text(the_k-0.6,  y1-top_ofs, "$n=1$", backgroundcolor='w')
            plt.plot([the_k, the_k], [y0, y1], 'k-.')


    if filename == None:
        hs = ("%5.3f" % h).replace('.', ',')
        filename = "disp_rel_h%s.eps" % hs
    plt.savefig(filename, transparent=True, bbox_inches='tight')
    if show:
        plt.show()
        pub.set_window(pub.GEOM_MONASH_UPPER_LEFT)


if __name__ == "__main__":
#    plot_relation(1.835, m1=9, m2=10, showm1=True, showm2=True,
#                  shift1_k = False, shift1_omega=False,
#                  retrograde=False,
#                  theta_deg=15., show=True, lmax=1, filename='disp_h1,835_m5.eps')
    plot_relation(2.667, m1=4, m2=5, shift1_k = True, shift1_omega=True,
                  theta_deg=3, show=True, lmax=4, filename=None, Po=0.361, retrograde=True)
#    plot_relation(1.62, m1=5, shift1_k = True, shift1_omega=True, theta_deg=0.7, show=False, lmax=1, filename='disp_h1,62_m156_origin.eps')
#    plot_relation(0.678, m1=4, showm1=True, showm2=True, shift1_k = True, shift1_omega=True, theta_deg=2, show=False, lmax=1, filename='disp_h0,678_m1.eps', ylim=[-1,3])
#    plot_relation(2.667, m1=4, showm1=True, showm2=True, shift1_k = True, shift1_omega=True, theta_deg=2, show=False, lmax=1, filename='disp_h2,667_m1.eps', ylim=[-1,3])
#    plot_relation(1.62, m1=5, showm1=False, showm2=False, shift1_k = True, shift1_omega=True, theta_deg=1, show=False, lmax=1, filename='disp_h1,62_m1.eps')
#    plot_relation(1.62, m1=5, shift1_k = True, shift1_omega=True, theta_deg=1, show=False, lmax=1, filename='disp_h1,62_m156_shift.eps')
#    plot_relation(1.62, m1=5, theta_deg=1, show=False, showm2=False, lmax=1, filename='disp_h1,62_m15.eps')
#    plot_relation(1.62, m1=5, theta_deg=1, show=False, showm2=False, showm1=False, lmax=1, filename='disp_h1,62_m1.eps')

    # -- get the two branches
    l1 = l2 = 1
    m2 = 6
    K1, OM1 = get_branch(l1, m=m2-1, kmax=10, negative=True)
    K2, OM2 = get_branch(l2, m=m2, kmax=10, negative=False)

    # -- move one branch
    h = 1.4
    om, k = get_om_k(h, n=1,  l=1, m=1)
    print om
    K1 += k
    OM1 += om
    plt.plot(K1, OM1, 'r-')
    plt.plot(K2, OM2, 'k-')

    #X = np.linspace(K1[0],10)
    #plt.plot(X, OM1_interp(X), 'r-')
    #plt.plot(X, OM2_interp(X), 'g-')
    k_intersect, om_intersect = get_intersect((K1, OM1), (K2, OM2))
    print "k_intersect", k_intersect, om_intersect
    plt.plot(k_intersect, om_intersect, 'rs')

    # -- plot exact resonances
    exact_resonances = [n * np.pi / h for n in range(10)]
    for the_k in exact_resonances:
        plt.plot([the_k, the_k], [y0, y1], 'k--')


    print "delta_k", get_delta_k(k_intersect, h)
    pub.SetPlotRC()
    plt.xlim(0, 10)
    plt.ylim(y0, y1)
    plt.xlabel("$k_2$")
    plt.ylabel("$\omega_2$")
    plt.savefig("detuned.eps", transparent=False, bbox_inches="tight")
    #plt.show()




