#!/usr/bin/env python
"""
read all case.*mdl, plot per-mode
(difference between time steps)
"""

#import numpy as np

import mdl
import pub
import os
import numpy as np
import argparse
import sys
from pdb import pm

def label_mode(m, i, T, E, text=None, x_offset=0):
    if not text:
        text = r'$\mathrm{%s}$' % m
    try:
        plt.text(T[i] + x_offset, E[m,i] * 1.5, text, color='k')
    except:
        pass

def tuple2(s):
    try:
        return map(float, s.split(','))
    except:
        raise argparse.ArgumentTypeError("tuple must be x,y")

case=os.path.basename(os.getcwd())


parser = argparse.ArgumentParser(description="")
parser.add_argument("-t0", default=0, type=float, help="")
parser.add_argument("-t1", type=float, help="")
parser.add_argument("-y0", default=None, type=float, help="")
parser.add_argument("-y1", default=None, type=float, help="")
parser.add_argument("-m", "--mode-labels", default=None, type=float, help="")
parser.add_argument("-i", "--interactive", action="store_true", help="interactive")
parser.add_argument("-a", "--auto-y", action="store_true", help="use autoscaling for y")
parser.add_argument("-g", "--gradient", action="store_true", help="show gradient of energy")
parser.add_argument("-k", "--no-black-lines", action="store_true", help="Don't mark 16,32,64... lines")
parser.add_argument("-l", "--linear", action="store_true", help="use linear y-axis")
parser.add_argument("-s", "--sqrt", action="store_true", help="plot sqrt(E)")
parser.add_argument("-p", "--png-only", action="store_true", help="output png only")
parser.add_argument("-e", "--eps-only", action="store_true", help="output eps only")
parser.add_argument("-n", "--non-dim-time", action="store_true", help="non-dim time by omega")
parser.add_argument("-N", "--non-dim-time-cyl", action="store_true", help="non-dim time by cyl rev")
parser.add_argument("-L", "--legend", action="store_true", help="show legend")
parser.add_argument("-X", "--no-x-labels", action="store_true", help="disable x labels")
parser.add_argument("-c", "--cut-mode", default=None, type=int, help="cut after given mode")
parser.add_argument("-f", "--figsize", type=tuple2, default=None, help="figure size (x,y)")
parser.add_argument("-v", "--vertical-lines", type=tuple2, default=None, help="add vertical lines at x1,x2,x3...")
parser.add_argument("-d", "--downsample", type=int, default=None, help="downsample factor")
parser.add_argument("-D", "--downsample-file", metavar="FILE", type=str, help="don't plot. Instead, read FILE, downsample, write to out/FILE.mdl.")
parser.add_argument("-1", "--normalise-modes", action="store_true", help="normalise each mode")
parser.add_argument("session", metavar='session[.mdl]', nargs='?', default=None,
                   help='session file')
args = parser.parse_args()

args.auto_y = True # was False for precession cases

import matplotlib
if not args.interactive:
    matplotlib.use("AGG") # prevent X11
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator

if args.downsample_file:
    if args.downsample is None:
        print "Need downsample factor (-d)"
        sys.exit(-1)
#    print args.downsample_file, args.downsample
    n_modes, T, E = mdl.load_mdl(args.downsample_file, every=args.downsample)

    out = np.vstack((T.repeat(E.shape[0]),
                    np.tile(np.arange(n_modes), len(T)),
                    E.transpose().flatten()))
    f = open(args.downsample_file, "w")
    f.write("#     Time Mode         Energy\n# ----------------------------\n")
    np.savetxt(f, out.transpose(), fmt="%1.10g %5i %1.17e")
#               np.vstack((T.repeat(E.shape[0]), E)))
    #mdl.save_mdl("out" + os.sep + args.downsample_file, n_modes, T, E)
    f.close()
    sys.exit(0)


pub.SetPlotRC()

if args.figsize:
    fig = plt.figure(figsize=args.figsize)

if args.session:
    case = args.session
else:
    if not os.path.exists(case):
        print "Default case (%s) not found. Please specify!" % case
        sys.exit(1)

if args.t0:
    plt.xlim(xmin=(args.t0))
if args.t1:
    plt.xlim(xmax=(args.t1))

pattern = case if case.endswith(".mdl") else "%s.*mdl" % case
n_modes, T, E = mdl.load_all_mdls(pattern, every=args.downsample)

if args.cut_mode != None and n_modes > args.cut_mode + 1:
    n_modes = args.cut_mode + 1
    E = E[0:n_modes]

# -- non-dim time
xlabel = '$t$'
T_ref = 1.
if args.non_dim_time:
    import dpiv_head as dh
    params = dh.DNSParams(case)
    T_ref = 1. / params.omega
    print params.Omega1, params.omega

elif args.non_dim_time_cyl:
    import dpiv_head as dh
    params = dh.DNSParams(case)
    T_ref = 2*np.pi / params.omega
    print params.Omega1, params.omega
    xlabel = '$t / T_1$'
T /= T_ref


if args.sqrt:
    E = np.sqrt(E)

if args.normalise_modes:
    for m in range(n_modes):
        E[m] /= E[m].max()

if 1:
    for m in range(10, n_modes):
        #plt.plot(T[1:], abs(np.diff(E[m])), 'k')
        if m % 16 == 15 and not args.no_black_lines:
            plt.plot(T, E[m], 'k')
        else:
            plt.plot(T, E[m], '0.75')

for m in range(min(10, n_modes)):
    plt.plot(T, E[m], color=pub.gp_palette[m], label="%i" % m)

# -- label modes
if args.mode_labels != None:
    time = args.mode_labels
    for i in range(len(T)):
        if T[i] > time:
            break
    label_mode(1, i, T , E, text='$m=1$')
    for m in range(0, 10):
        if m != 1:
            label_mode(m, i, T, E, x_offset=0)


if args.gradient:
    # show derivative of modal energy -- shows better picture of growth/decay
    every = 100
    T = T[::every]
    E = E[:,::every]
    plt.plot(T, np.gradient(E[1]), "^", color=pub.gp_palette[1], label="E1'", mec="None")
    plt.plot(T, np.gradient(E[0]), "^", color=pub.gp_palette[0], label="E0'", mec="None")
    plt.plot(T, np.gradient(E[5]), "^", color=pub.gp_palette[5], label="E5'", mec="None")

    plt.plot(T, -np.gradient(E[1]), "v", color=pub.gp_palette[1], label="-E1'", mec="None")
    plt.plot(T, -np.gradient(E[0]), "v", color=pub.gp_palette[0], label="-E0'", mec="None")
    plt.plot(T, -np.gradient(E[5]), "v", color=pub.gp_palette[5], label="-E5'", mec="None")


if args.linear:
    plt.ylim(0, E.max())
    print E.max()
else:
    plt.semilogy()
    #plt.ylim(1e-20,1)
    if not args.auto_y:
        plt.ylim(1e-40,1)

    #plt.ylim(1e-20, 1e-2)

if args.y0:
    plt.ylim(ymin=(args.y0))
if args.y1:
    plt.ylim(ymax=(args.y1))

if args.legend:
    plt.legend(loc='right')

if args.vertical_lines:
    for t in args.vertical_lines:
        t /= T_ref
        plt.plot([t,t], plt.gca().get_ylim(), '--', color='0.1')


# -- auto minor x ticks
minorLocator = AutoMinorLocator()
plt.gca().xaxis.set_minor_locator(minorLocator)

if args.no_x_labels:
    pub.unset_xtics()
else:
    plt.xlabel(xlabel)
plt.ylabel('$E_m$')

if args.interactive:
    plt.show()
else:
    if not args.eps_only:
        plt.savefig('%s.modal_energy.png' % case)
    if not args.png_only:
        plt.savefig('%s.modal_energy.eps' % case, transparent=True)

