#!/usr/bin/env python

import numpy as np
import sys

def mk_bins(half_len, step):
    """Bins are symmetric about 0: (2,1) creates edges [-1.5, -0.5, 0.5, 1.5]"""
    bins = np.arange(step/2., half_len, step)
    bins = np.hstack((-bins[::-1], bins, bins[-1]+step))
    mids = bins - step/2.
    return bins[:-1], mids


def test_mk_bins():
    b, m = mk_bins(2, 1)
    assert((b == [-1.5, -0.5, 0.5, 1.5]).all() == True)
    assert((m == [-2, -1, 0, 1, 2]).all() == True)


class Histogram(object):
    """Similar to np.histogram2d(), but store content of bins, and allow access to it.
    """
    def __init__(self, omegas, sigmas, bins):
        self.bins_omega, self.bins_sigma = bins

        ## 3 edges make 4 bins
        self.n_omega = len(self.bins_omega) + 1
        self.n_sigma = len(self.bins_sigma) + 1

        ## coord-sys-like access: x first, then y
        self.bins = [[[] for s in xrange(self.n_sigma)] for o in xrange(self.n_omega)]
        self.H = np.zeros((self.n_omega, self.n_sigma))

        self.add(omegas, sigmas)
        self.compute_H()

    def index(self, omegas, sigmas):
        o = np.digitize(omegas, self.bins_omega)
        s = np.digitize(sigmas, self.bins_sigma)
        return o, s

    def count_at(self, omega, sigma):
        """Return count of bin which (omega, sigma) falls into."""
        # FIXME: do we need iter variant?
        o, s = self.index([omega], [sigma])
        return self.H[o, s]

    def add(self, omegas, sigmas):
        o, s = self.index(omegas, sigmas)
        for i, (the_omega, the_sigma) in enumerate(zip(omegas, sigmas)):
            self.bins[o[i]][s[i]].append((the_omega, the_sigma))

    def show(self):
        print "# bins omega", self.bins_omega
        print "# bins sigma", self.bins_sigma
        print self.n_omega, self.n_sigma
        print "# transposed:"
        for o in xrange(self.n_omega):
            print self.bins[o]

    def compute_H(self):
        self.H = np.array([[len(self.bins[o][s]) for s in xrange(self.n_sigma)] for o in xrange(self.n_omega)])

    def show_H(self):
        # FIXME: pretty-print
        print self.H


#def clamp(n, smallest, largest):
#    return max(smallest, min(n, largest))

def test_Histogram():
    bins_omega = [2]
    bins_sigma = [0, 2]
    bs = Histogram([-2, -1, 2.1], [-3, -3, -3], [bins_omega, bins_sigma])
    bs.add([3, 3], [1, 20])
    print bs.bins_omega
    bs.show()
    bs.compute_H()
    bs.show_H()
    assert ((bs.H == np.array([[2, 0, 0], [1, 1, 1]])).all() == True)


if __name__ == "__main__":
    test_Histogram()
    test_mk_bins()
