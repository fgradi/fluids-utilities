#!/usr/bin/env python

# - load .dat
# - extract max
# - filter outlier?
# - plot vs time

import numpy as np
import sys
import matplotlib.pyplot as plt
from pdb import pm
import pub
import uniread
import glob
import string
from numpy import cos, sin, pi, exp
import os
import skimage
import skimage.io
import scipy.signal
import query



if 0:
    # -- Taylor-Green vortex
    x = np.linspace(0, 2*pi, 51)
    X, Y = np.meshgrid(x, x)
    U =  cos(X)*sin(Y)
    V = -sin(X)*cos(Y)
    plt.quiver(X, Y, U, V)
    plt.show()
    bla

skip=1
pub.mkdirs('movie/')
pub.mkdirs('prf/')
files = glob.glob("dat/*0.dat")
files.sort()
files = files[::skip]
for the_file in files:
    print the_file

plot=False
case = os.path.basename(os.getcwd())
last_two = string.join(os.getcwd().split("/")[-2:], "/")
print "#l1", os.getcwd().split("/")[-2:]
print "#lt", last_two
D = query.query_pwd()
start_frame = D['start_frame']

def read_field(the_file):
    num = int(string.split(the_file, '_')[-1][:-4])
    U = uniread.uniread(the_file)
    # -- filter flag
    mask = [U.flag == 4]
    U.u[mask] == 0.
    U.v[mask] == 0.

    return U, num

def plot_quiver(mesh, u, v, save_num=None):
#        plt.clf()
        plt.axes().set_aspect('equal', 'datalim')
        plt.ylim(0, 20)
#        plt.axes().set_aspect('equal')

#        plt.contourf(U.x, U.y, U.u)
#        plt.contourf(U.x, U.y, U.flag)
        plt.quiver(mesh.ax, mesh.ay, u, v, scale=40)
#        plt.colorbar()
#        plt.show()
#        bla
        if save_num:
            plt.savefig('movie/%08i.png' % save_num)


Ulast, tmp = read_field(files[-1])
mesh = Ulast
# -- to enable plotting against index, not coord
mesh.ax = np.arange(len(mesh.x))
mesh.ay = np.arange(len(mesh.y))

nfiles = len(files)

if not os.path.exists("Uall.npy"):
    Uall = np.zeros((len(files), Ulast.ny, Ulast.nx))
    Vall = np.zeros((len(files), Ulast.ny, Ulast.nx))

    # -- read in all files
    for i, the_file in enumerate(files):
        U, num = read_field(the_file)
        Uall[i] = U.u
        Vall[i] = U.v
        print i, the_file

    # -- save to file
    np.save("Uall", Uall)
    np.save("Vall", Vall)
else:
    Uall = np.load("Uall.npy")[::skip]
    Vall = np.load("Vall.npy")[::skip]

# -- moving average in time
Upost = Uall
Vpost = Vall
if 0:
    N = 6 # which means we need at least this many snapshots!
    print "# moving average kernel size", N
    onesN = np.ones((N,))/N
    tmp = [np.convolve(hist, onesN, mode='same') for hist in Uall.reshape(nfiles, -1).transpose()]
    Upost = np.array(tmp).transpose().reshape(Uall.shape)
    tmp = [np.convolve(hist, onesN, mode='same') for hist in Vall.reshape(nfiles, -1).transpose()]
    Vpost = np.array(tmp).transpose().reshape(Uall.shape)

if 1:
    N = 3
    print "# median filter kernel size", N
    Upost = scipy.signal.medfilt(Upost, kernel_size=[N, 1, 1])
    Vpost = scipy.signal.medfilt(Vpost, kernel_size=[N, 1, 1])

if 0:
    # -- Lamb-Oseen vortex of core radius rc, located at yc
    yc = 45.6
    rc = 10
    Gamma = 400.
    y = mesh.y
    LambOseen = Gamma / (2*np.pi*(y-yc)) * (1-np.exp( -((y-yc)**2/(rc)**2) ))



if 0:
    for i in np.arange(1500, nfiles, 10):
        plt.plot(mesh.y, Uall[i,:,74], color='0.5')
    #plt.plot(mesh.y, LambOseen)
#    plt.plot(Uall[:,:,74].mean(axis=0), color='r') # can't be right!
    plt.ylim(-8,8)
#    plt.plot(Upost[-20,:,74])
    plt.xlabel("y")
    plt.ylabel("horizontal vel")
    plt.show()


def frame_num(the_file):
    return int(the_file.split('_')[-1][:-4])

frame_nums = [frame_num(the_file) for the_file in files]


# -- plot vectors
#fig = plt.figure(frameon=False, figsize=(14,3))
import importruntime as ir
ROI = ir.importRuntime("roi.py")
fig, (axi, axv, axcu, axcv, axh) = plt.subplots(nrows=5, ncols=1, figsize=(10,15))
print files[-1]

stem = string.join(os.path.basename(files[0]).split('_')[:-1], '_')

# -- query DB
dist = int(D['dist'])

if 0:
    mid = len(files)/2
    files = files[mid:mid+5]

has_cb = False

for i, the_dat in enumerate(files):
    # - Our .dat is imgA. imgB = imgA + dist
    #   Plot centre .tif = dat + dist/2
    dat_i = frame_num(the_dat)

    tif_i = dat_i + dist / 2 + 50
    the_tif = "tif/%s_%08i.tif" % (stem, tif_i)

    try:
        img = skimage.io.imread(the_tif)
        img = skimage.img_as_ubyte(img[ROI.y0:ROI.y1, ROI.x0:ROI.x1])
        axi.imshow(img, cmap="gray")
    except IOError, reason:
        print "# skipping", the_tif, reason

    # -- vector plot
    axv.set_xlim(0, mesh.ax[-1])
    axv.axes.set_aspect('equal', 'datalim')
    axv.quiver(mesh.ax, mesh.ay, Upost[i], Vpost[i], scale=60, color='r')

    # -- plot median u/v displacement
    median_u = np.median(abs(Upost[i].ravel()))
    median_v = np.median(abs(Vpost[i].ravel()))
    max_u = max(Upost[i].ravel())
    max_v = max(Vpost[i].ravel())
    axv.text(1.01, 0.5, 'median\n|u|=%4.2f\n|v|=%4.2f' % (median_u, median_v),
             transform=axv.transAxes)
    axv.text(1.01, 0, 'max   \nu=%4.2f\nv=%4.2f' % (max_u, max_v),
             transform=axv.transAxes)

    probe = (D['px'], D['py'])
    # -- plot histogram
    if 0:
        axh.hist(Upost[i].ravel(), 50, range=[-20, 20], color="k", alpha=0.5)
        axh.hist(Vpost[i].ravel(), 50, range=[-20, 20], color="r", alpha=0.5)
        axh.set_xlim(-20, 20)
    else:
        # -- plot probe
        y0 = -10
        y1 = 10
        axh.plot(frame_nums, Vall[:, probe[1], probe[0]])
        axh.plot(frame_nums, Vpost[:, probe[1], probe[0]])
        # -- start frame marker
        axh.vlines(int(start_frame), y0, y1, 'r')
        axh.vlines(dat_i, y0, y1)
        axh.set_ylim(y0, y1)
#        axh.xlabel("frame number")
#        axh.ylabel("horizontal vel")
        axv.plot(probe[0], probe[1], 'ro')

    # -- plot u,v contours
    levels = np.linspace(-10, 10, 21)
    cb = axcu.contourf(mesh.ax, mesh.ay, Upost[i], levels=levels)
    if not has_cb:
        has_cb = fig.colorbar(cb, orientation="vertical", shrink=1.0, fraction=0.03, pad=0.06, ax=axcu)
    cb = axcv.contourf(mesh.ax, mesh.ay, Vpost[i], levels=levels)

    # -- plot probe marker
    axcu.plot(probe[0], probe[1], 'ro')
    axcv.plot(probe[0], probe[1], 'ro')

    axh.text(int(start_frame), y0, "%i" % int(start_frame))

    axv.text(0, 1.01, os.path.basename(the_dat) + " + %i" % dist, transform=axv.transAxes)
    axi.text(0, 1.01, os.path.basename(the_tif), transform=axi.transAxes)

    plt.savefig('movie/%08i.png' % i)
    if i % 10 == 0:
        print i
    axi.cla()
    axv.cla()
    axh.cla()
    axcu.cla()
    axcv.cla()
