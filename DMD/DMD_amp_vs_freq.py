#!/usr/bin/env python

import numpy as np
import sys
import matplotlib.pyplot as plt
from pdb import pm
import pub
import glob
import os
import mdl
import parse_string
import DMD_io

class Mode(object):
    def __init__(self, D, i):
        self.omega_normd = D.omega_normd[i]
        self.amp = D.amp[i]
        self.sigma = D.sigma[i]
        self.m = D.m[i]
        #self. = D.[i]
        #self. = D.[i]


def plot3(logs, w1, possible_m):
    shift = 10.
    EPS = 0.025
    y0 = 1e-4
    y1 = 1
#    y0 = 1e-9
#    y1 = 1e-8
    for the_log in logs:
        D = DMD_io.Data_full(the_log, use_sparse=0, normalise=1)
        parsed = parse_string.Parse_String(the_log, ['R','w','c','s', 't'])
#        theta.append(parsed.t)

        D.omega_normd = D.omega / parsed.w
        D.omega_normd += shift

        # -- sort modes into bins
        modes = [[] for the_w in w1]
        modes_cc = [[] for the_w in w1]
        modes_rest = []
        for i, omega_normd in enumerate(D.omega_normd):
            mode = Mode(D, i)
            if mode.omega_normd > 19.5: continue
            for j, the_w in enumerate(w1):
                found = False
                if abs(omega_normd % 1 - the_w) < EPS:
                    modes[j].append(mode)
                    found = True
                    break
                elif abs(omega_normd % 1 - (1-the_w)) < EPS:
                    modes_cc[j].append(mode)
                    found = True
                    break
            if not found:
                modes_rest.append(mode)

#        fig, ax = plt.subplots(nrows=len(w1)+1, ncols=1, sharex=True, squeeze=True)
#        fig, ax = plt.subplots(nrows=1, ncols=1, sharex=True, squeeze=True)
        for j, the_w in enumerate(w1):
            plt.clf()
            #the_ax = ax[j]
            fig, ax = plt.subplots(nrows=1, ncols=1, sharex=True, squeeze=True)
            the_ax = ax

            the_ax.semilogy()
            the_ax.set_ylabel(the_w)
            the_ax.set_ylabel("amp")
            the_ax.set_xlabel(r"$\omega / \omega_F$")
            the_ax.set_ylim(y0, y1)
#            the_ax.set_xlim(0, 20)

            top_mode_i = 0
            for i, the_mode in enumerate(modes[j]):
                col = pub.gp_palette[possible_m[j]]
                sym = pub.symbols[possible_m[j]]
                the_mode.omega_normd -= shift
                the_ax.plot(the_mode.omega_normd, the_mode.amp, sym, ms=10, color=col)
                label = r"$%i$" % the_mode.m
                the_ax.text(the_mode.omega_normd+0.05, the_mode.amp, label, color=col)

                # -- find largest mode...
                find_largest_mode = False
                if find_largest_mode:
                    if the_mode.amp > modes[j][top_mode_i].amp:
                        top_mode_i = i

            # ... and mark it with a circle
            if find_largest_mode:
                top_mode = modes[j][top_mode_i]
                the_ax.plot(top_mode.omega_normd, top_mode.amp, 'ro', ms=10, mfc="None")

            if 1: # -- vertical lines
#                y0, y1 = the_ax.get_ylim()
                for i in range(-10, 10):
                    the_ax.vlines(i+the_w, y0, y1, color='0.75')

            # -- plot CC'd
            if 0:
                alpha = 0.15
                for the_mode in modes_cc[j]:
                    col = pub.gp_palette[j]
                    the_ax.plot(the_mode.omega_normd, the_mode.amp, 'o', color=col, alpha=alpha)
                    label = r"$%i$" % the_mode.m
                    the_ax.text(the_mode.omega_normd+0.05, the_mode.amp*0.9, label, color=col, alpha=alpha)

            plt.savefig("frac_%5.3f.png" % the_w)
            plt.close()

        plt.savefig("full_spectrum.eps")

        # -- rest goes to -1
        if 0:
            for the_ax in ax[:-1]:
            #the_ax = ax[-1]
                for the_mode in modes_rest:
                    col = '0.5'
                    the_ax.plot(the_mode.omega_normd, the_mode.amp, 'o', mfc="None", color=col)
                    label = r"$%i$" % the_mode.m
                    the_ax.text(the_mode.omega_normd+0.05, the_mode.amp, label, color=col)

        the_ax = ax[-1]
        the_ax.semilogy()
        the_ax.set_ylim(y0, y1)
        the_ax.set_xlim(0, 20)
        for the_mode in modes_rest:
            col = '0.5'
            the_ax.plot(the_mode.omega_normd, the_mode.amp, 'o', mfc="None", color=col)
            label = r"$%i$" % the_mode.m
            the_ax.text(the_mode.omega_normd+0.05, the_mode.amp, label, color=col)


    print np.array([len(item) for item in modes]).sum()
    print np.array([len(item) for item in modes_cc]).sum()
    print len(modes_rest)



def hist(the_log):
    D = Data(the_log, use_sparse=0, normalise=1)
    parsed = parse_string.Parse_String(the_log, ['R','w','c','s', 't'])
    plt.hist((D.omega/parsed.w + 10) % 1, bins=400)
    plt.xlabel(r"$(\omega / \omega_F)$ $\mathrm{modulo}$ $1.$")
    plt.ylabel("count")
    plt.savefig("hist.eps")
    pub.set_window(pub.GEOM_MONASH_UPPER_LEFT)
    bla



if __name__ == "__main__":

    pub.SetPlotRC()
    if 1:
        plt.clf()
        plt.xlabel(r"$\alpha$")
        plt.ylabel("$a_m/max(a)$")

     # ------------- low alpha ---------------
    #logs = ["nomoreSFD_h2,667_t00,6_R006500_k1_l1_w0,735_N05_z0064_c52_s100_p1.log.0,01"]
    logs = ["nomoreSFD_h2,667_t03,0_R006500_k1_l1_w0,735_N05_z0064.52,4.cyl.log"]
    #hist(logs[0]) # cool!


    w1 = [0, 0.06, 0.0979, 0.198, 0.321, 0.395, 0.456]
    possible_m = [0, 11, 6, 5, 9, 0, 8]
    plot3(logs, w1, possible_m)
#    plot(logs, w1, w1, color='b', mfc='b', label_col='b', show_vlines=True, only_valid=1)
    pub.set_window(pub.GEOM_MONASH_UPPER_LEFT)
    #    plt.show()
