#!/usr/bin/env python

PROG="a.py"
USAGE="new.sh file"
DESC="<One line of description>"
HELP="<Zero or more lines of Help>"
AUTHOR="Thomas Albrecht"

import numpy as np
import sys
import matplotlib.pyplot as plt
import loadbn
from pdb import pm
import pub
import glob
import skimage.io
import copy
files = glob.glob("*.tif")
files.sort()
#files = files[::skip]

fig, (axi) = plt.subplots(nrows=1, ncols=1, figsize=(10,15))

img = skimage.io.imread(files[0])
blank = skimage.img_as_int(img)
blank = np.zeros_like(blank*1.)


def avg(files, fname):
    out = copy.copy(blank)
    navg = 0
    for the_tif in files:
        img = skimage.io.imread(the_tif)
    #    img = skimage.img_as_ubyte(img[ROI.y0:ROI.y1, ROI.x0:ROI.x1])
        img = skimage.img_as_ubyte(img)
        out += img
        navg += 1

    axi.imshow(out/navg, cmap="gray")
    plt.show()

    skimage.io.imsave(fname, skimage.img_as_ubyte(out/navg/255.))

#avg(files[:10], "avg.tif")
avg(files, "all.tif")

# loop all tifs.
#   - average n_avg
#   - identify particles
#   - match particles
#   - get trajectory
#