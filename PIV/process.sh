#!/bin/bash

# PIV and post-process Amy's experiments.
# HDD should be mounted on $src_drive, see below.
# run from ./
# In each case dir, we need:
# - mask.tif
# - roi.py

# - parameters? could need to be different for some cases?
# 
# seems like 15dbm case is too fast for dist=500. Try 200.


PROG=process.sh
USAGE="$PROG file"
DESC="<One line of description>"
HELP="<Zero or more lines of Help>"
AUTHOR="Thomas Albrecht"

# [ "$1" == "" ] || [ "$1" == "-h" ] || [ "$1" == "--help" ] && \
#     echo -e "Usage: $USAGE\n$DESC\n$HELP" && exit 1

set -u
#set -x  # print commands before executing
#set -e  # exit if single command fails


src_drive="/media/albrecht/Recovered Volume/Transient study"

# Experiments should be registered in cases.txt, one per line.
# Assumed to be relative to $src_drive path
#cases=cases.txt
cases="
B1D3_375/B1D3W2_5_180MHz_10dBm
B1D3_375/B1D3W2_10_180MHz_15dBm
B1D3_375/B1D3W2_14_180MHz_19dBm
B1D3_750/B1D3W3_10_180MHz_15dBm
B1D3_750/B1D3W3_14_180MHz_19dBm
B1D3_750/B1D3W3_5_180MHz_10dBm
B1D4_187p5/B1D4W1_10_180MHz_15dBm
B1D4_187p5/B1D4W1_14_180MHz_19dBm
B1D4_187p5/B1D4W1_5_180MHz_10dBm
B1D4_375/B1D4W2_10_180MHz_15dBm
B1D4_375/B1D4W2_14_180MHz_19dBm
B1D4_375/B1D4W2_5_180MHz_10dBm
"
# cases="B1D4_375/B1D4W2_5_180MHz_10dBm"

bd=$PWD
# process all cases given in cases.txt
for line in $cases; do
# while read line; do
    [ ${line:0:1} == "#" ] && continue

    
    cd $bd
    case="${src_drive}/${line}"
    echo
    echo "================================================="
    echo $line
    ls -1d "$case"
    mp4=`echo $line | tr '/' '_'`
    echo $mp4
    echo "================================================="

    cd "$case"
    #cp $bd/_masks/$line/roi.py "$case/" # -- copy ROI from _masks
    #ls -1 roi.py
    #continue
    
    # -- rename spaces to underscores in file names
    if [ 1 == 0 ]; then
        emv "*[0-4].tif" " " "_" do
        emv "*[5-9].tif" " " "_" do
        rm emv.??????
        mkdir -p tif/
        mv *.tif tif/
        find . -maxdepth 1 -name '*.tif' -exec mv {} tif/ \;

        mv tif/mask.tif .
    continue
    fi
    
    # -- save first .tif
    #mkdir -p "$bd/$case/"
    #cp `ls -1 *.tif | head -n1` "$bd/$case/"
    #continue

    # -- pre-process: copy mask, .par etc
    cp -H $bd/template/* "$case/"

    # -- clean up beforehand
    #rm dat/*
    #rm movie/* 
    rm movie/scaled/*

    #continue
    
    # -- run PIV. Clean up afterwards: all tifs will go to tif/
#     rm dat/* movie/*.png;     continue
    ndat=`ls dat/ | wc -l`
    echo "ndat" $ndat
    [ $ndat -le 10 ] && /bin/bash ./pivntecit.serial.bash

    # -- run post-processing.
    rm *.npy
    set -e
    [ ! -s movie/00000001.png ] && /usr/bin/python ./post.py
    cd movie/
    echo "MOVIE"
    #pwd
    #echo "mp4:<$mp4>"
    rm -f $mp4.mp4
    mkvideo.mencoder.sh
    mv *.mp4 "$mp4.mp4"
    cp "$mp4.mp4" $bd
    cd ..
    set +e
done

