#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
In 2015JFM, we seek steady solutions to mode 0 of NSE. Two forcing terms appear, through which
higher modes couple into mode 0.
1. R, due to NL term of
2. C, due to Coriolis term.

However, explaining the flow structure turned out to be difficult. One issue is
the pressure gradient term. We now try to base our analysis on the vorticity
transport equation, as it eliminates the pressure gradient term.

Since we have the velocity field u, we can simply compute all terms of the NSE,
then take the curl using addfield.
- compute NL term of NSE in python.
- use addfield to calc curl
- same for viscous term, Coriolis etc
This gets us all terms of the VTE, which we can then plot.

Another option is to compute omega = curl(u), then compute all terms of the VTE.
- requires to compute nu * laplace(omega), ie. need to code for vector laplacian.

- input:
  - u,

TODO:
x automatically run grad u (addvelgrad output)
- run again to get selected second derivatives
- copy&paste from coriolis.py
x write down viscous term in cylindrical. Get from Navier.pdf
- computeNSEterms?

Checked:
x addvelgrad works. Checked d/dx, d/dy, d/dz in cylindrical.
x SBR in cyl frame, i.e. alpha = 0, cylinder rotating upright in cyl frame.
  u=0 everywhere, so Coriolis is zero. Centrifugal term is balanced by pressure gradient.
  Check OK.

- same, but derived from existing Cyl frame session file, remove time dependency!



"""
#
#
#
# compute terms of vorticity transport equation
# by Thomas Albrecht
#

import numpy as np
import sys
from pdb import pm
import argparse
import fieldfile
import copy
from pdb import pm
import string
import subprocess
import matplotlib.pyplot as plt


def load_ff(fld_fname, required_fields):
    """load given fld_name, return a data_dict.
       Complain if it doesn't match required_fields
    """
    ff = fieldfile.Fieldfile(fld_fname, "r")
    data = ff.read()
    #elmt_wise = data.reshape((ff.nr, ff.ns, ff.nel, ff.nz, ff.nflds))
    data_dict = {}
    for i, field in enumerate(ff.fields):
        data_dict[field] = data[i]
    assert (string.join(ff.fields, "") == required_fields)
    return data, data_dict, ff


def get_jacobian(U, session, fld_fname):
    # FIXME: could save some memory here: since addvelgrad returns uvw,
    #        we could accept this field file instead of the uvw .fld
    """
    addvelgrad:
    [a b c]   [u_x  u_y  u_z]
    [d e f] = [v_x  v_y  v_z]
    [g h i]   [w_x  w_y  w_z]
    """
    jacobian_fname = "tmp_jacU.chk"
    jacobian = open(jacobian_fname, "w")
    result = subprocess.call(['addvelgrad', '-s', session, fld_fname],
                             stdout=jacobian)
    jacobian.close()
    ff = fieldfile.Fieldfile(jacobian_fname, "r")
    data = ff.read()
    data_dict = {}
    for i, field in enumerate(ff.fields):
        if field in 'abcdefghi':
            data_dict[field] = data[i]

    JU = np.vstack((data[0], data[1], data[2],
                    data[3], data[4], data[5],
                    data[6], data[7], data[8])).reshape(3,3,-1)

    return JU, jacobian_fname




def cross(A, B):
    """double checked"""
    return np.array([A[1] * B[2] - A[2] * B[1],
                     A[2] * B[0] - A[0] * B[2],
                     A[0] * B[1] - A[1] * B[0]])


def mat_derivative_cyl(A, B, jacobian_B, Y):
    """compute material derivative in cylindrical coordinates
    Input: vector fields A, B
           Jacobian of B
           radial coord Y
    Gives the same answer as nl_term().
    """
    JB = jacobian_B
    A_dot_grad_B = np.zeros_like(A)

    A_dot_grad_B[0] = A[1] * JB[0,1] + A[2] * JB[0,2] / Y + A[0] * JB[0,0] # OK OK
    A_dot_grad_B[1] = A[1] * JB[1,1] + A[2] * JB[1,2] / Y + A[0] * JB[1,0] - A[2] * B[2] / Y # OK OK
    A_dot_grad_B[2] = A[1] * JB[2,1] + A[2] * JB[2,2] / Y + A[0] * JB[2,0] + A[2] * B[1] / Y # OK OK

    return A_dot_grad_B

def nl_term(U, jacobian_U, Y):
    JU = jacobian_U
    N = np.zeros_like(U)
    # -- FIXME: still wrong
#    N[0] = -0.5*(U[0]*JU[0,0] + U[1]*JU[0,1] +  2*U[0]*JU[0,0]         + (JU[1,1]*U[0] + U[1]*JU[0,1]) + 1/Y*(U[2]*JU[0,2] + (JU[0,2]*U[2] + U[0]*JU[2,2]) + U[1]*U[0]        ))
#    N[1] = -0.5*(U[0]*JU[1,0] + U[1]*JU[1,1] +  (JU[0,0]*U[1] + U[0]*JU[1,0]) + 2*U[1]*JU[1,1]         + 1/Y*(U[2]*JU[1,2] + (JU[1,2]*U[2] + U[1]*JU[2,2]) + U[1]*U[1] - 2*U[2]*U[2]))
#    N[2] = -0.5*(U[0]*JU[2,0] + U[1]*JU[2,1] +  (JU[0,0]*U[2] + U[0]*JU[2,0]) + (JU[1,1]*U[2] + U[1]*JU[2,1]) + 1/Y*(U[2]*JU[2,2] + 2*U[2]*JU[2,2]               + 3*U[2]*U[1]))

    N[0] = U[1] * JU[0,1] + U[2] * JU[0,2] / Y + U[0] * JU[0,0]
    N[1] = U[1] * JU[1,1] + U[2] * JU[1,2] / Y + U[0] * JU[1,0] - U[2]**2 / Y
    N[2] = U[1] * JU[2,1] + U[2] * JU[2,2] / Y + U[0] * JU[2,0] + U[1] * U[2] / Y

    return N


def coriolis_cyl(U, alpha, omega, mesh):

    Omega1 = omega
    Omega2 = (1.0-omega)/np.cos(alpha)

    #mean_OMEGA_x = Omega2 * np.cos(alpha) # in Cartesian
    #mean_OMEGA_z = Omega2 * np.sin(alpha)

    # -- cyl frame, in Cartesian
    cyl_frame = False
    if cyl_frame:
        Omega = np.array([Omega1 + Omega2 * np.cos(alpha),
                          0.,
                          Omega2 * np.sin(alpha)])
    else:
        Omega = np.array([Omega2 * np.cos(alpha),
                          0.,
                          Omega2 * np.sin(alpha)])



    # -- project to cylindrical coordinates. This differs by plane.
    Omega_cyl_x = Omega[0]
    Omega_cyl_y = np.zeros_like(U[0]).reshape((mesh.geometry.nz, mesh.geometry.nel, mesh.geometry.ns, mesh.geometry.nr))
    Omega_cyl_z = np.zeros_like(Omega_cyl_y)
    for k in range(mesh.geometry.nz):
        phi = k * 2. * np.pi / mesh.geometry.nz
        Omega_cyl_y[k,:,:,:] =  np.cos(phi) * Omega[1] + np.sin(phi) * Omega[2]
        Omega_cyl_z[k,:,:,:] = -np.sin(phi) * Omega[1] + np.cos(phi) * Omega[2]

    Omega_cyl_y = Omega_cyl_y.flatten()
    Omega_cyl_z = Omega_cyl_z.flatten()
    Omega_cyl = [Omega_cyl_x, Omega_cyl_y.ravel(), Omega_cyl_z.ravel()]

    Cori = -2*cross(Omega_cyl, U)

    # -- centrifugal -- double checked OK
    R = [mesh.X, mesh.Y, mesh.Z*0.]
    Cent = -np.array(cross(Omega_cyl, cross(Omega_cyl, R)))

#    bla

    return Cori, Cent


def curl_cyl(A, jacobian_A, Y):
    """compute the curl of vector field A in cylindrical coordinates"""
    curl_A = np.zeros_like(A)
    JA = jacobian_A

    curl_A[1] = 1/Y *  JA[0,2] - JA[2,0]
    curl_A[2] =        JA[1,0] - JA[0,1]
    curl_A[0] = 1/Y * (A[2] + Y * JA[2,1] - JA[1,2])


def stats(term, description=""):
    print description
    for i in range(3):
        finite = term[i][np.isfinite(term[i])]
        print "  %s %11.3e %11.3e %11.3e" % ("xyz"[i], finite.min(),
                                                    finite.max(), finite.std())

def stats1(term, description=""):
    print description
    finite = term[np.isfinite(term)]
    print "  ", finite.min(), finite.max(), finite.std()


def viscous_cyl(mu, U, jacobian_U, diag_hess_U, Y):
    """
    Compute visous term of NSE in cylindrical coordinates.
    We need second derivatives of U, given by

                  [u_xx  u_yy  u_zz]
    diag_hess_U = [v_xx  v_yy  v_zz]
                  [w_xx  w_yy  w_zz]

    diag_hess_U[0,2] = u_zz

    CHECKED diag_hess: d/dx OK d/dy OK d/dz OK
    """
    JU = jacobian_U
    HU = diag_hess_U
    YY = Y*Y
    Visc = np.zeros_like(U)

#    Visc[0] = uy/Y        + uyy + 1/YY*uzz           + uxx
#    Visc[1] = vy/Y - v/YY + vyy + 1/YY*vzz - 2/YY*wz + vxx
#    Visc[2] = wy/Y - w/YY + wyy + 1/YY*wzz + 2/YY*vz + wxx
    Visc[0] = JU[0,1]/Y           + HU[0,1] + HU[0,2]/YY                 + HU[0,0]
    Visc[1] = JU[1,1]/Y - U[1]/YY + HU[1,1] + HU[1,2]/YY - 2./YY*JU[2,2] + HU[1,0]
    Visc[2] = JU[2,1]/Y - U[2]/YY + HU[2,1] + HU[2,2]/YY + 2./YY*JU[1,2] + HU[2,0]

    return Visc * mu


def plot_field(field, mesh, file_name, zplane=0, ax=None, rnge=None, cb=True, disable_ax_labels=True):
    if ax is None:
        fig = plt.figure(frameon=False, figsize=(4*0.45, 5.5*0.45))
        plt.clf()
        ax = fig.gca()

    X = mesh.X.reshape((mesh.geometry.nz, mesh.geometry.nel, mesh.geometry.ns, mesh.geometry.nr))[0]
    Y = mesh.Y.reshape((mesh.geometry.nz, mesh.geometry.nel, mesh.geometry.ns, mesh.geometry.nr))[0]
    F = field.reshape((mesh.geometry.nz, mesh.geometry.nel, mesh.geometry.ns, mesh.geometry.nr))[zplane]
    if rnge:
        lmin = rnge[0]
        lmax = rnge[1]
    else:
        lmin = F[np.isfinite(F)].min()
        lmax = F[np.isfinite(F)].max()
    levels = np.linspace(lmin, lmax, 12)

    if lmin == lmax:
        for i in xrange(mesh.geometry.nel):
            cf = ax.contourf(Y[i], X[i], F[i], label="w", extend="both")
    else:
        #levels=np.linspace(-0.0018, 0.001, 15)
        for i in xrange(mesh.geometry.nel):
            cf = ax.contourf(Y[i], X[i], F[i], levels=levels, label="w", extend="both")

    if file_name:
        ax.set_title(file_name)

    if cb:
        plt.colorbar(cf, ax=ax)

    ax.set_xlim(-0.01, 0.51)
    ax.set_ylim(-0.45, 0.45)
#
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)


def write(U, ff, fields, fname, keep_open=True):
    hdr = copy.copy(ff.hdr)
    hdr.fields = fields
    ff_out = fieldfile.Fieldfile(fname, 'w', hdr)
    ff_out.write_fields(U, keep_open=keep_open)


def addfield_curl(U, ff, session):
    """compute curl of given U using addfield"""
    fi = open("tmpi", "wb")
    fo = open("tmpo", "wb+")
    write(U, ff, "uvw", fi, keep_open=True)
    fi.flush()
    p1 = subprocess.Popen(["addfield", "-s", session, "-v", "/dev/stdin"],
                          stdin=fi, stdout=subprocess.PIPE)
    p2 = subprocess.Popen(["ff.py", "-K", "rst", "/dev/stdin"], stdin=p1.stdout,
                          stdout=fo)
    p1.stdout.close()  # Allow p1 to receive a SIGPIPE if p2 exits.
    output, err = p2.communicate()
    fo.seek(0)
    return load_ff(fo, "rst")

if __name__ == "__main__":
    """
    input: velocity field

    need
      uvw
      jacobian u
    and some second derivatives:

    create from within python? yep.


    """
    parser = argparse.ArgumentParser(description="mk_coriolis")
    parser.add_argument("-x", '--x-only', action="store_true", help="plot")
    parser.add_argument("-z", '--z-only', action="store_true", help="plot")
    parser.add_argument("-c", '--centri-only', type=str, metavar="MESH", help="plot")
#    parser.add_argument("alpha", type=float, help="tilt angle, deg")
#    parser.add_argument("omega", type=float, help="omega")
    Re = 1.
    if 0:
        parser.add_argument('infile', metavar='file', type=str, nargs=1,
                       help='the file')
        args = parser.parse_args()

        fld_fname = args.infile[0]
    else:
#        fld_fname = "SBR.fld" # -- OK
#        fld_fname = "arbitSBR.fld" # -- OK
#        fld_fname = "gradP.fld" # -- OK
#        fld_fname = "h1,620_t01,0_R006500_k1_l1_w1,180_N05_z0032.fld"; mu = 0.25 / 6500.
#        fld_fname = "h0,457_t00,35_R006500_k1_l1_w1,886_N05_z0032.fld";  mu = 0.25 / 6500.
         #fld_fname = "cylkov1.fld"; mu = 1/10. # probably good enough
 #       fld_fname = "pipe1.fld"; mu = 1/100. # OK, but tests only one component
#        fld_fname = "diag_hess.fld"
        # CHECK: /mnt/scr4/talbrech/iwave/vort_transport/h0,457_t02,0_R001000_k1_l1_w1,886_N07_z0064

        # OK: residual = 1e-2
        # res
        #  x  -4.033e-04   4.033e-04   7.793e-06
        #  y  -3.211e-04   3.675e-04   8.075e-06
        #  z  -1.007e-02   1.308e-02   1.719e-04
        #fld_fname = "h0,457_t02,0_R001000_k1_l1_w1,886_N07_z0064.fld"; mu = 0.25 / 1000.; alpha_deg=2.; omega = 1.886
        #fld_fname = "drive_AL_h1,620_t00,4_R006500_w1,181_N07_z001_twice.fld"; mu = 0.25/6500.; alpha_deg = 0.4; omega = 1.181
        fld_fname = "drive_AL_h1,620_t00,4_R006500_w1,181_N07_z001.fld"; mu = 0.25/6500.; alpha_deg = 0.4; omega = 1.181
        frc_fname = "drive_AL_h1,620_t00,4_R006500_w1,181_N07_z001.frc.chk"
        # increase N_Z and N_P
        #res
        #  x  -2.118e-04   2.118e-04   3.472e-06
        #  y  -1.891e-04   1.924e-04   3.315e-06
        #  z  -1.008e-02   1.310e-02   1.325e-04
#        fld_fname = "h0,457_t02,0_R001000_k1_l1_w1,886_N09_z0128.fld"; mu = 0.25 / 1000.; alpha_deg=2.

        # OK: residual = 1e-13
        #fld_fname = "cyl_h0,457_t00,0_R001000_k1_l1_w1,886_N05_z0032.fld"; mu = 0.25 / 1000.; alpha_deg=0.
#        fld_fname = "cyl_SBR.fld"; mu = 0.25 / 1000.
        #fld_fname = "cyl_SBR2.fld"; mu = 0.25 / 1000.
    session = fld_fname.split('.')[0]

#NEXT: try with MSF cases, take curl
    # - for each term:
    #      write to file, use addfield to calculate curl.
    #      (Mhh: what about BC? Does addfield use vel BC given in session file?)

    alpha = np.deg2rad(alpha_deg)


    # -- read mesh
    import semesh
    import pub
    pub.SetPlotRC(font_size=10)
    mesh_fname = session + ".mesh"
    mesh = semesh.Semesh(mesh_fname)
    mesh.to_3d()
    #mesh.Y[mesh.Y == 0] = 1e-99

    # -- load U. Check that uvw exist.
    U, data_dict, ff = load_ff(fld_fname, "uvwp")
    U = U[0:3] # drop pressure

    # -- load Jacobian and second derivatives
    jacobian_U = load_ff(session + ".jac.chk", "abcdefghi")[0].reshape(3,3,-1)
    U_xx =  load_ff(session + ".U_xx.chk", "adg")[0]
    U_yy =  load_ff(session + ".U_yy.chk", "beh")[0]
    U_zz =  load_ff(session + ".U_zz.chk", "cfi")[0]
    diag_hess_U = np.hstack((U_xx, U_yy, U_zz)).reshape(3,3,-1)

    # -- load gradP, check. works OK.
    gradP = -load_ff(session + ".gradP.chk", "abc")[0]
    gradP[2] /= mesh.Y

    # -- load vorticity
    W, data_dict, ff = load_ff(session + ".rst.chk", "rst")

    #mu = 1.
    Visc = viscous_cyl(mu, U, jacobian_U, diag_hess_U, mesh.Y)
    NL1 = mat_derivative_cyl(U, U, jacobian_U, mesh.Y)
    NL = nl_term(U, jacobian_U, mesh.Y)

    Cori, Cent = coriolis_cyl(U, alpha, omega, mesh)

    # -- forcing
    F = load_ff(frc_fname, "uvw")[0]
    #F = np.zeros_like(NL) # general body force

    Res = NL - (gradP + Visc + Cori + Cent + F)
#    Res = NL - (gradP + Visc)

    stats(Res, "res")
    stats(NL, "NL")
    stats(gradP, "gp")
    stats(Visc, "visc")
    stats(Cori, "Cori")
    stats(Cent, "Cent")
    stats(F, "F")

    if 1:
        fig, ax = plt.subplots(3,7, figsize=(15,8), dpi=300)
        plt.tight_layout(pad=1.0, w_pad=1.5, h_pad=1.0)
        ax = ax.transpose()
        zplane = 0
        for i in range(3):
            axis = ' ' + 'xyz'[i]
            rnge = [(-0.011, 0.011), (-0.11, 0.11), (-0.11, 0.11)][i]
            rnge = None
            plot_field(NL[i], mesh, "NL" + axis, zplane, ax=ax[0,i], rnge=rnge)
            plot_field(gradP[i], mesh, "dp" + axis, zplane, ax=ax[1,i], rnge=rnge)
            plot_field(Visc[i], mesh, "visc" + axis, zplane, ax=ax[2,i], rnge=rnge)
            plot_field(Cori[i], mesh, "Co" + axis, zplane, ax=ax[3,i], rnge=rnge)
            plot_field(Cent[i], mesh, "Cent" + axis, zplane, ax=ax[4,i], rnge=rnge)
            plot_field(F[i],  mesh, "F" + axis, zplane, ax=ax[5,i], rnge=rnge)
            plot_field(Res[i],  mesh, "Res" + axis, zplane, ax=ax[6,i], rnge=rnge, cb=True)

    #    print NL - (-gradP + Visc + Cori + Cent)
        plt.savefig("terms.eps")
        plt.show()
        bla

    # -------------------------------------------------------------------------
    # curl
    # -------------------------------------------------------------------------

    # -- x CHECK if addfield_curl(visc) == visc(omega)
    #write(Visc, ff, "uvw", session + ".visc.chk")
    # compute curl of visc term using addfield
    #curl_visc = load_ff(session + ".curl_visc.chk", "rst")[0]
    curl_Visc = addfield_curl(Visc, ff, session)[0]

    # -- compute the viscous term of the VTE
    jacobian_W = load_ff(session + ".jacW.chk", "abcdefghi")[0].reshape(3,3,-1)
    if 0:
        W_xx =  load_ff(session + ".W_xx.chk", "adg")[0]
        W_yy =  load_ff(session + ".W_yy.chk", "beh")[0]
        W_zz =  load_ff(session + ".W_zz.chk", "cfi")[0]
        diag_hess_W = np.hstack((W_xx, W_yy, W_zz)).reshape(3,3,-1)
        visc_W = viscous_cyl(mu, W, jacobian_W, diag_hess_W, mesh.Y)

        write(Visc, ff, "uvw", session + ".visc.chk")




    # -- curl2(NL) = (U . grad) W - (W . grad) U
    curl_NL2l = mat_derivative_cyl(U, W, jacobian_W, mesh.Y) # on LHS
    curl_NL2r = mat_derivative_cyl(W, U, jacobian_U, mesh.Y) # on RHS

    curl_Cori = addfield_curl(Cori, ff, session)[0]
    curl_NL = addfield_curl(NL, ff, session)[0]
    curl_Cent = addfield_curl(Cent, ff, session)[0]
    curl_gradP = addfield_curl(gradP, ff, session)[0]
    curl_F = addfield_curl(F, ff, session)[0]

    #Res = NL - (gradP + Visc + Cori + Cent)
    #Res2 = curl_NL - (curl_gradP + curl_Visc + curl_Cori + curl_Cent)
    Res2 = curl_NL2l - (curl_NL2r + curl_gradP + curl_Visc + curl_Cori + curl_Cent + curl_F)
    stats(Res2, "Res2")

    stats(curl_Cent, "curl_Cent")
    stats(curl_gradP, "curl_gradP")

    print "plotting"
    if 1:
        fig, ax = plt.subplots(3,8, figsize=(15,8), dpi=300)
        plt.tight_layout(pad=1.0, w_pad=1.5, h_pad=1.0)
        ax = ax.transpose()
        zplane = 0
        for i in range(3):
            axis = ' ' + 'xyz'[i]
            #rnge = [(-0.02, 0.02), (-0.2, 0.2), (-0.2, 0.2)][i]
            rnge = [(-0.011, 0.011), (-0.11, 0.11), (-0.11, 0.11)][i]

            plot_field(curl_NL2l[i], mesh, "(u.grad)w" + axis, zplane, ax=ax[0,i], rnge=rnge)
            plot_field(curl_NL2r[i], mesh, "(w.grad)u" + axis, zplane, ax=ax[1,i], rnge=rnge)
            plot_field(curl_gradP[i], mesh, "curl dp" + axis, zplane, ax=ax[2,i], rnge=rnge)
            plot_field(curl_Visc[i], mesh, "curl visc" + axis, zplane, ax=ax[3,i], rnge=rnge)
            plot_field(curl_Cori[i], mesh, "curl Co" + axis, zplane, ax=ax[4,i], rnge=rnge)
            plot_field(curl_Cent[i], mesh, "curl Cent" + axis, zplane, ax=ax[5,i], rnge=rnge)
            plot_field(curl_F[i], mesh, "curl F" + axis, zplane, ax=ax[6,i], rnge=rnge)
            plot_field(Res2[i],  mesh, "Res" + axis, zplane, ax=ax[7,i], rnge=rnge, cb=True)

    #    print NL - (-gradP + Visc + Cori + Cent)
        plt.savefig("curl_terms.eps")
        plt.show()

    print "done"
