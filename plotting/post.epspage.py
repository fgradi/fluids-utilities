#!/usr/bin/env python2
"""Use pdfLaTeX or LaTeX to arrange many images.
(c) 2015-2017 Thomas Albrecht
"""

import sys
import os
import string
import logging
import textwrap
#from pdb import pm
import copy

known_extensions = {"pdflatex": [".eps", ".png", ".jpg", ".pdf"], "latex": [".eps"]}
REG_MAGIC = "REGISTER12345"


class Img_params(object):
    """Store parameters for a single image"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.width = None
        self.height = None
        self.angle = None
        self.prefix = ""
        self.postfix = ""
        self.picture_mode = False
        self.show_name = False
        self.override_opts = None

class Image(object):
    def __init__(self, file_name, params, doc):
        self.file_name = file_name
        self.base_name, self.extension = os.path.splitext(self.file_name)
        if string.lower(self.extension) not in known_extensions[doc.processor]:
            logging.warning('Unknown image extension %s in "%s". %s may fail.'
                         % (self.extension, self.file_name, doc.processor))
        self.params = params
        self.is_image = True


class Change(object):
    def __init__(self, parameter, value):
        self.parameter = parameter
        self.value = value


class Command(object):
    def __init__(self, command, arg=None):
        self.command = command
        self.arg = arg


def first_not_none(candidates):
    for c in candidates:
        if c: return c
    return None


def convert_to_pdf(obj):
    file_name, extension = os.path.splitext(obj.file_name)
    if extension.lower() == '.eps':
        os.system('epstopdf %s' % obj.file_name)
    obj.file_name = file_name + '.pdf'
    return obj


class Document(object):
    def __init__(self):
        self._objects = []
        self.processor = 'latex'
        self.is_portrait = True
        self.case_name = "post"
        self.margin_cm = 0.1
        self.current_img = None
        self.draft = False
        self.rm_tmp_pdf = True
        self.tmp_pdf_list = []
        self.ignore_missing = False

        # -- defaults
        self.defaults = Img_params()
        self.max_img_x = 0
        self.max_img_y = 0

        self._cur_img_x = 0
        self._cur_img_y = 0

    def append(self, image):
        self._objects.append(image)
        self.current_img = image

    def change_defaults(self, parameter, value):
        self._objects.append(Change(parameter, value))

    def command(self, command, arg=None):
        self._objects.append(Command(command, arg))

    def clearpage(self):
        self._cur_img_x = 0
        self._cur_img_y = 0
        return "\n\clearpage\n%\n"

    def newline(self):
        self._cur_img_x = 0
        self._cur_img_y += 1
        if self.max_img_y > 0 and self._cur_img_y >= self.max_img_y:
            return self.clearpage()
        else:
            return "\\\\\n% newline\n"

    def string(self, arg):
        s = str(arg)
        s = s.replace('_', '\_')
        s = s.replace('%', '\%')
        return "%s" % s

    def make_length(self, length):
        try:
            s = '%g\\tw' % float(length)
        except:
            s = str(length)
        return s

    def picture_mode_header(self):
        s = "\\begin{picture}(0,0)\n  \put(0,0){"
        return s

    def picture_mode_footer(self):
        s = "}\n\end{picture}"
        return s

    def sanitize_string(self, string):
        def insert(string, index):
            return string[:index] + '\+' + string[index:]
        for i in range(0, len(string), 2)[::-1]:
            string = insert(string, i)
        return string.replace('_', '\_')

    def write(self):
        """open latex file, writer"""
        out = open(self.case_name + ".tex", "w")
        print "Mcm", self.margin_cm
        if self.is_portrait:
            out.write(textwrap.dedent("""
            \NeedsTeXFormat{LaTeX2e}
            \documentclass[a4paper,oneside,10pt]{article}
            \usepackage[margin=%gcm,nohead]{geometry}
            %% \setlength{\\topmargin}{-0.5cm}
            %% \setlength{\headheight}{0cm}
            %% \setlength{\headsep}{0cm}
            %% \setlength{\evensidemargin}{-1.04cm}
            %% \setlength{\oddsidemargin}{-1.04cm}
            %% \setlength{\\textwidth}{18cm}
            %% \setlength{\\voffset}{0.0cm}
            %% \setlength{\hoffset}{0.0cm}
            %% \setlength{\\textheight}{25cm}
            """ % self.margin_cm))
        else:
            out.write(textwrap.dedent("""
            \NeedsTeXFormat{LaTeX2e}
            \documentclass[a4paper,oneside,10pt,landscape]{article}
            \usepackage[margin=%gcm,nohead,landscape,dvips]{geometry}
            %% \setlength{\\topmargin}{-0.5cm}
            %% \setlength{\headheight}{-1.5cm}
            %% \setlength{\headsep}{0cm}
            %% \setlength{\evensidemargin}{-1.04cm}
            %% \setlength{\oddsidemargin}{-1.04cm}
            %% \setlength{\\textwidth}{26cm}
            %% \setlength{\\voffset}{0.0cm}
            %% \setlength{\hoffset}{0.0cm}
            %% \setlength{\\textheight}{17cm}
            """ % self.margin_cm))

        # -- common header
        if self.draft:
            out.write("\usepackage[draft]{graphicx}")
        else:
            out.write("\usepackage{graphicx}")

        out.write(textwrap.dedent("""
        \\newcommand{\\tw}{\\textwidth}
        \usepackage[absolute]{textpos}
        \usepackage[utf8x]{inputenc}
        \usepackage{amsmath,bm}
        \setlength{\parindent}{0cm}
        \\newcommand{\zerobox}[1]{%
        \\begin{textblock}{0}(0.5,0)%
        \mbox{#1}%
        \end{textblock}}%
        \def\+{\discretionary{}{}{}}

        \\begin{document}
        """))

        for obj in self._objects:
            if hasattr(obj, 'file_name'):  # -- Image
                if self.ignore_missing:
                    if not os.path.exists(obj.file_name) or os.path.getsize(obj.file_name) == 0:
                        logging.info("skipping non-existing file %s" % (obj.file_name))
                        continue

                logging.info("writing image %s" % (obj.file_name))

                if obj.extension == '.eps' and self.processor != 'latex':
                    obj = convert_to_pdf(obj)
                    self.tmp_pdf_list.append(obj.file_name)

                if obj.params.picture_mode:
                    out.write(self.picture_mode_header())

                show_name = first_not_none([obj.params.show_name, self.defaults.show_name])
                opts = ""
                width = first_not_none([obj.params.width, self.defaults.width])
                if width:
                    if show_name:
                        opts += "width=\\tw,"
                    else:
                        opts += "width=" + self.make_length(width) + ','

                height = first_not_none([obj.params.height, self.defaults.height])
                if height:
                    opts += "height=" + self.make_length(height) + ','

                angle = first_not_none([obj.params.angle, self.defaults.angle])
                if angle:
                    opts += "angle=" + angle + ','

                if opts.endswith(','):
                    opts = opts[:-1]
                    
                override_opts = first_not_none([obj.params.override_opts, self.defaults.override_opts])
                if override_opts is not None:
                    opts = override_opts

                prefix = first_not_none([obj.params.prefix, self.defaults.prefix])
                postfix = first_not_none([obj.params.postfix, self.defaults.postfix])
                s = ""
                if show_name:
                    s += "\\begin{minipage}[t]{%s}\n" % self.make_length(width)
                if prefix:
                    s += self.string(prefix)
                s += "\includegraphics[%s]{{%s}%s}" % (opts, obj.base_name, obj.extension)
                if postfix:
                    s += self.string(postfix)
                if show_name:
                    s += "\\\\\n%s\n" % self.sanitize_string(os.path.basename(obj.base_name))
                    s += "\end{minipage}"

                out.write(s)

                # -- picture mode? Add hspace to account for width
                if obj.params.picture_mode:
                    out.write(self.picture_mode_footer())
                    out.write('\hspace*{%s}' % self.make_length(width))

                self._cur_img_x += 1
                if self.max_img_x > 0 and self._cur_img_x >= self.max_img_x:
                    out.write(self.newline())
                else:
                    out.write('\n')

            elif hasattr(obj, 'parameter'):  # -- change
                parameters = ["max_img_x", "max_img_y"]
                default_params = ["width", "height", "prefix", "postfix", "show_name", "angle", "override_opts"]
                logging.info("changing parameter %s to %s" % (obj.parameter, str(obj.value)))
                if obj.parameter in parameters:
                    self.__dict__[obj.parameter] = obj.value
                elif obj.parameter in default_params:
                    self.defaults.__dict__[obj.parameter] = obj.value
                    # print "sn now", self.defaults.show_name
                else:
                    logging.error('Unknown parameter: %s' % obj.parameter)
                    raise ValueError

            elif hasattr(obj, 'command'):  # -- command
                commands = ['newline', 'string', 'clearpage']
                if obj.command in commands:
                    logging.info("command %s" % obj.command)
                    if obj.arg:
                        out.write(getattr(self, obj.command)(obj.arg))
                    else:
                        out.write(getattr(self, obj.command)())
                else:
                    logging.error('Unknown command: %s(%s)' % (obj.command, str(obj.arg)))
                    raise ValueError

        out.write(textwrap.dedent("""
        \end{document}
        """))

        out.close()

    def cleanup(self):
        if self.rm_tmp_pdf:
            for pdf in self.tmp_pdf_list:
                try:
                    os.remove(pdf)
                except OSError:
                    pass


def check_option(arg):
    value = None
    if arg.startswith('-'):
        if arg[1] == '-':  # -- long option
            arg = arg[1:]
        option = arg[1:].split('=')[0]
        try:
            value = string.join(arg.split('=')[1:], '=')
            if (value.startswith("'") and value.endswith("'")) or \
               (value.startswith('"') and value.endswith('"')):
                value = value[1:-1]
        except IndexError:
            pass
        return option, value
    else:
        return False, False


class Myparse(object):
    def __init__(self):
        self.helpstring = ""

    def suggest_help(self):
        print "Try -H or --help for help."

    def help(self):
        print textwrap.dedent("""Use pdfLaTeX or LaTeX to arrange many images into a multi-page pdf.

        Command line arguments are processed in order. Each argument either
        - specifies an image to print next,
        - is a command (e.g. line break) to be inserted at the current position,
        - changes the options for the next image (e.g. width), or
        - changes the default options for all upcoming images.

        Options:
        """)
        print self.helpstring
        print textwrap.dedent("""      Examples:

        Put two images on a page, stacked vertically:
          -x=1 -y=2 a.png b.png

        Same effect, but also add a description:
          -s='two images' -n a.png -n b.png

        You can set default values for width, height, rotation angle etc.:
          -dw=0.2 *.png
        Defaults are effective immediately, but never change images already processed.
        Note that -x adjusts the default width. You might want to use -dw AFTER -x.

        The default length unit is \\textwidth. If no width etc. is given, pdfLaTeX
        will figure it out.

        Specifying values per image overrides defaults:
          -dw=0.2 a.png -w=0.5 b.png c.png
        a.png and c.png will be 0.2 wide, while b.png will be wider.
        """)

    def add_to_helpstring(self, short_name, long_name, needs_value, desc=""):

        wrapped = textwrap.wrap(desc, 50)
        if not wrapped: wrapped = ['']
        self.helpstring += "  -%-5s --%-16s %s\n" % \
          (short_name + needs_value, long_name + needs_value, wrapped[0])

        if len(wrapped) > 1:
            for l in wrapped[1:]:
                self.helpstring += " " * 28 + l + '\n'

    def is_known(self, opt_val, short_name, long_name, desc=""):
        option, value = opt_val
        if long_name.endswith('#'):
            long_name = long_name[:-1]
            needs_value = "=#"
        else:
            needs_value = ""

        if option == REG_MAGIC:
            self.add_to_helpstring(short_name, long_name, needs_value, desc)
            return False

        if option == short_name or option == long_name:
            if needs_value and value is None and option != REG_MAGIC:
                logging.error('option -%s (--%s) needs an argument.' % (short_name, long_name))
                self.suggest_help()
                sys.exit(1)

            return True
        else:
            return False


def main():
    do_pdf = True

    nxt = Img_params()
    parse = Myparse()
    args = sys.argv
    args[0] = "--" + REG_MAGIC
    if 0:
        args = "--" + REG_MAGIC + " " + "-w=0.3 -dw=0.1 -x=3 -y=2 -dr=always_pre -r=pre -p=post 00001.png -s=bla_bla -l --output=bb A0.png -e A0_vel.png A0_vel.png A0_vel.png A0_vel.png -w=0.4 A0_vel.png A0_vel.png"
        args = args.split()

    doc = Document()
    img = None
    for arg in args:
        option, value = check_option(arg)
        if option:
            if value:
                logging.info('option %s = %s' % (option, value))
            else:
                logging.info('option %s' % option)
            opt_val = (option, value)

            if parse.is_known(opt_val, 'h', 'help', desc="show this help"):
                parse.help()
                return

            elif parse.is_known(opt_val, 'o', 'output#', desc="set output case name. Default=post"):
                doc.case_name = value
            elif parse.is_known(opt_val, 'm', 'margin_cm#', desc="set margin in cm. Default=0.1cm"):
                doc.margin_cm = float(value)
            elif parse.is_known(opt_val, 'x', 'x#', desc="automatically newline after # images. This sets default_width to 1/#."):
                doc.change_defaults("max_img_x", int(value))
                doc.change_defaults("width", 1./float(value))

            elif parse.is_known(opt_val, 'y', 'y#', desc="automatically clearpage after # rows of images"):
                doc.change_defaults("max_img_y", int(value))
            elif parse.is_known(opt_val, 'd', 'draft', desc="enable [draft] option of graphicx package, speeding up compilation. Default=off"):
                doc.draft = True
            elif parse.is_known(opt_val, 'l', 'landscape', desc="Use landscape orientation. Default=portrait"):
                doc.is_portrait = False
            elif parse.is_known(opt_val, 'k', 'keep-pdf', desc="keep temporary .pdf files generated from .eps. Default=remove."):
                doc.rm_tmp_pdf = False

            elif parse.is_known(opt_val, 'w', 'width#', desc="width of next image"):
                nxt.width = float(value)

            elif parse.is_known(opt_val, 'H', 'height#', desc="height of next image"):
                nxt.height = float(value)

            elif parse.is_known(opt_val, 'a', 'angle#', desc="rotation angle of next image"):
                nxt.angle = float(value)
            elif parse.is_known(opt_val, 'pr', 'pre#', desc="prefix. Insert this string before next image"):
                nxt.prefix = str(value)
            elif parse.is_known(opt_val, 'po', 'post#', desc="postfix. Insert this string after next image"):
                nxt.postfix = str(value)

            elif parse.is_known(opt_val, 'dw', 'default-width#'):
                doc.change_defaults("width", float(value))
                doc.change_defaults("height", None)

            elif parse.is_known(opt_val, 'dh', 'default-height#'):
                doc.change_defaults("height", float(value))
                doc.change_defaults("width", None)

            elif parse.is_known(opt_val, 'da', 'default-angle#'):
                doc.change_defaults("angle", str(value))

            elif parse.is_known(opt_val, 'dO', 'default-options#', desc="override entire \includegraphics options string"):
                #doc.override_opts = value
                doc.change_defaults("override_opts", value)

            elif parse.is_known(opt_val, 'dpr', 'default-pre#'):
                doc.change_defaults("prefix", str(value))
            elif parse.is_known(opt_val, 'dpo', 'default-post#'):
                doc.change_defaults("postfix", str(value))

            elif parse.is_known(opt_val, 'ds', 'default-show-name'):
                doc.change_defaults("show_name", True)
            elif parse.is_known(opt_val, 'dn', 'default-no-name'):
                doc.change_defaults("show_name", False)

            elif parse.is_known(opt_val, 'p', 'picture', desc="use picture environment for next image. Allows image to overlap with previous row."):
                nxt.picture_mode = True

            elif parse.is_known(opt_val, 'n', 'newline', desc="manual line break"):
                doc.command('newline')
            elif parse.is_known(opt_val, 'N', 'newpage', desc="manual page break"):
                doc.command('clearpage')

            elif parse.is_known(opt_val, 's', 'string#', desc="insert string"):
                doc.command('string', value + ' ')  # FIXME: value should respect ''

            elif parse.is_known(opt_val, 'D', 'dry-run', desc="Don't run pdfLaTeX. Useful for checking command line arguments."):
                do_pdf = False
            elif parse.is_known(opt_val, 'i', 'ignore-missing', desc="ignore missing files. Default=off"):
                doc.ignore_missing = True
            elif parse.is_known(opt_val, 'P', 'pdflatex', desc="Use pdfLaTeX. Automatically converts eps to pdf."):
                doc.processor = 'pdflatex'

            elif option == REG_MAGIC:
                pass
            else:
                logging.error("Unknown option %s." % arg)
                parse.suggest_help()
                sys.exit(-1)

        else:
            #splitted = a.split(':') # possible future use of filename.ext:param:
            #img_name = splitted[0]
            img_name = arg
            img = Image(img_name, copy.copy(nxt), doc)
            doc.append(img)
            nxt.reset()

    if len(args) == 1:
        parse.help()
        print "Nothing to do. Bye!"
        sys.exit(0)

    doc.write()
    if do_pdf:
        os.system('%s %s.tex' % (doc.processor, doc.case_name))
        if doc.processor == 'latex':
            os.system('dvipdf %s' % doc.case_name)
    doc.cleanup()

if __name__ == "__main__":
    logging.basicConfig(level=logging.WARNING)
    main()

# TODO: possible future options:
#  -s #, --scale #       scale images (default 1.0)
#  -n  , --no-label      do not print label
#  -p  , --skip-path     do not print path in label
#  -f  , --first-title   use name of first file as title for each page
#  -e #, --endl #        append string # at the end of a row
#  -m  , --movie         movie-mode: only print name of first image in row
#  -c  , --col-major     Fill column first, then proceed to next row.
#                        Default is to fill a row first, the proceed to next column.
#  -b #, --break         pagebreak after # images
#  -d #, --description # print description text in header
