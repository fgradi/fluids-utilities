#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Python library for reading and writing semtex field files.
(c) 2011-2017 Thomas Albrecht

Semtex is Hugh Blackburn's spectral element-Fourier flow solver (Blackburn & Sherwin JCP 2004).
Semtex stores flow field variables in field files (.fld or .chk), which consist
of a ten line ASCII header, followed by a---usually binary---field dump.
The field dump consists of a number of fields of equal length, one per flow variable.

Usage:
    read:

    ff = Fieldfile("example.fld", "r")
    data = ff.read()
    elmt_wise = data.reshape((ff.nflds, ff.nz, ff.nel, ff.ns, ff.nr))

    write:

    ff = Fieldfile("example.fld", "w")
    ff.write(data)

"""


import string
import numpy as np
import sys


def components_cyl_to_cart(vec_field, mesh):
    U =  vec_field[0]
    V =  vec_field[1] * np.cos(mesh.Z) + vec_field[2] * np.sin(mesh.Z)
    W = -vec_field[1] * np.sin(mesh.Z) + vec_field[2] * np.cos(mesh.Z)

    return np.vstack((U, V, W))


class Geometry(object):

    """Data structure for field file geometry"""

    def __init__(self, nr=3, ns=3, nz=1, nel=1):
        self.nr = nr
        self.ns = ns
        self.nz = nz
        self.nel = nel
        self.nxy = nr * ns * nel

    def __str__(self):
        return "%i %i %i %i" % (self.nr, self.ns, self.nz, self.nel)

    def __eq__(self, other):
        """Compare, return """
        is_equal = True
        msg = ""
        if self.nr != other.nr:
            msg += " nr differs"
            is_equal = False
        if self.ns != other.ns:
            msg += " ns differs"
            is_equal = False
        if self.nz != other.nz:
            msg += " nz differs"
            is_equal = False
        if self.nel != other.nel:
            msg += " nel differs"
            is_equal = False

        return is_equal, msg


class Header(object):

    def __init__(self, geometry, fields="", format="binary"):
        self.session = ""
        self.created = ""
        self.geometry = geometry
        self.step = 0
        self.time = 0.
        self.dt = 0.
        self.kinvis = 1.
        self.beta = 1.
        self.fields = fields
        self.format = format

    def read(self, f):
        hdr = []
        for i in range(0, 9):
            hdr.append(string.split(f.readline()))
        hdr.append(f.readline())

        self.session = hdr[0][0]
        self.created = hdr[1][:-1]
        self.geometry = Geometry(int(hdr[2][0]), int(hdr[2][1]),
                                 int(hdr[2][2]), int(hdr[2][3]))
        self.step = int(hdr[3][0])
        self.time = np.double(hdr[4][0])
        self.dt = float(hdr[5][0])
        self.kinvis = float(hdr[6][0])
        self.beta = float(hdr[7][0])
        self.fields = hdr[8][0]
        self.format = hdr[9][0:25]

    def write(self, f):
        f.write(str(self))

    def __str__(self):
        out = []
        out.append("%-25s Session" % self.session)
        out.append("%-25s Created" % "")
        out.append("%-4i %-4i %-4i %-6i     Nr, Ns, Nz, Elements" %
                   (self.geometry.nr, self.geometry.ns, self.geometry.nz, self.geometry.nel))
        out.append("%-25i Step" % self.step)
        out.append("%-25.15g Time" % self.time)
        out.append("%-25g Time step" % self.dt)
        out.append("%-25g Kinvis" % self.kinvis)
        out.append("%-25g Beta" % self.beta)
        out.append("%-25s Fields written" % self.fields)
        out.append("%-25s Format" % self.format)
        return string.join(out, '\n') + '\n'

# ------------------------------------------------------------------------------
class Fieldfile(object):

    def __init__(self, fname, state='r', header=None, read_data=False):
        """Open a field file for reading or writing.

        :param fname: file name string. If "-", read from stdin or write to stdout.
        :param state: 'r' or 'w'
        :param header: Header object, required when state == 'w'
        :param read_data: boolean, if True and state == 'r', attempt to read actual data.
        """
        if state == "r":
            if fname == '-':
                self.f = sys.stdin
            else:
                try:
                    self.f = open(fname, "r")
                except TypeError:
                    self.f = fname
            self.hdr = Header(Geometry())
            self.hdr.read(self.f)

        elif state == "w":
            if not header:
                raise ValueError('Need header when writing file')
            self.hdr = header
            if fname == '-':
                self.f = sys.stdout
            else:
                try:
                    self.f = open(fname, "w")
                except TypeError:
                    self.f = fname
            self.hdr.write(self.f)

        # FIXME: this design sucks. Shouldn't replicate data.
        #        Get rid of hdr?
        self.nr = self.hdr.geometry.nr
        self.ns = self.hdr.geometry.ns
        self.nz = self.hdr.geometry.nz
        self.nel = self.hdr.geometry.nel

        self.nrns = self.hdr.geometry.nr * self.hdr.geometry.ns
        self.nxy = self.nrns * self.hdr.geometry.nel
        self.ntot = self.nxy * self.hdr.geometry.nz
        self.nflds = len(self.hdr.fields)
        self.ntotf = self.ntot * self.nflds

        self.data = None

        # -- create list of field variables
        self.fields = [f for f in self.hdr.fields]

        self.data_dict = {}

        if read_data:
            self.read()

    # --------------------------------------------------------------------------
    def write(self, data, keep_open=False):
        """Write field data to file.

        :param data: a block of float64 data.
        :keep_open: boolean. Unless True, close file after writing data.
        """
        if data.dtype != np.dtype('float64'):
            print "typ is", data.dtype
            raise TypeError('need float64 data')
        data.tofile(self.f)
        if not keep_open:
            self.f.close()

    def write_fields(self, fields):
        """Write list of fields to file

        :param fields: a list of fields
        """
        self.write(np.vstack(the_field.flatten() for the_field in fields))

    def __getitem__(self, fieldname):
        return self.data[self.field_index(fieldname)]

    # --------------------------------------------------------------------------
    def read(self, reshape=True):
        """read field data from file. Populates data_dict. Float64 data expected.

        :param reshape: boolean. If True, reshape into fields.
        """

        buf = self.f.read()
        self.data = np.fromstring(buf, np.float64, count=-1)
        if not reshape:
            return self.data

        self.data = self.data.reshape(self.nflds, self.ntot)
        # return np.fromfile(self.f, 'd')

        for i, field in enumerate(self.fields):
            self.data_dict[field] = self.data[i]

        return self.data

    # --------------------------------------------------------------------------
    def reread(self, reshape=True):
        self.f.seek(0)
        for j in range(10):  # -- skip header
            self.f.readline()
        return self.read(reshape)

    def alloc(self):
        """allocate and return data storage"""
#        if not fields: fields = self.hdr.fields
        return np.zeros((self.ntotf), dtype='float64')

    # --------------------------------------------------------------------------
    def close(self):
        self.f.close()

    def field_index(self, needle):
        """Return index of given field
        :param needle: A character, the field.
        """
        return self.fields.index(needle)

    def is_mesh_compatible(self, mesh):
        return mesh.geometry == self.hdr.geometry

# ----------------------------------------------------------


def convert():
    """convert fieldfile to ASCII, like utility/convert.C"""
    ff = Fieldfile("example.fld", "r")
    ff.hdr.write(sys.stdout)
    data = ff.read()

    for i in range(ff.ntot):
        for field in range(ff.nflds):
            print "%g" % data[i, field],
        print


def demo_element_wise():
    """Demonstrates element-wise access.
       NB: Fieldfile.read() expects double precision (Float64) data, as is
       standard for semtex field files. We don't check for single precision or
       funny byte order.
    """
    ff = Fieldfile("example.fld", "r")
    data = ff.read()
    elmt_wise = data.reshape((ff.nflds, ff.nz, ff.nel, ff.ns, ff.nr))  # works!
    #    print data
    #    - of nr * ns nodes
    #    - of 11th element
    #    - of first z-plane
    #    - of second field (in this case, v)
    print elmt_wise[:, :, 10, 0, 1]

if __name__ == "__main__":
    demo_element_wise()
    # convert()
    # main()
