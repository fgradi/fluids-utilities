#!/usr/bin/env python
"""A lightweight wrapper for np.loadtxt(), allows to access columns by name.

Usage:
    data = loadbn("A0.dat", headerstr='time aspect theta Reynolds om azero')
    print data.Reynolds  # prints 4th column

(c) 2016-2017 Thomas Albrecht
"""
import numpy as np
import matplotlib.pyplot as plt


class loadbn(object):
    """
    Load a set of columns from given file. Store them by name.
    Names are extracted from headerstr if given, or from first
    line in file otherwise.
    """
    def __init__(self, filename, dtype=float, headerstr=None, comments='#', delimiter=None,
                 converters=None, usecols=None, skiprows=0):
        if not headerstr:
            f = open(filename, 'r')
            headerstr = f.readline()
            f.close()

        header = headerstr.lstrip('# ').rstrip('\n').replace('\t', ' ').split(' ')
        header = [self.cleanup(item) for item in header if len(item) > 0]

        data = np.loadtxt(filename, dtype=dtype, comments=comments, delimiter=delimiter,
                          converters=converters,
                          usecols=usecols, skiprows=skiprows).transpose()

        if usecols:
            header = [item for i,item in enumerate(header) if i in usecols]
            self.usecols = usecols
        else:
            self.usecols = range(len(header))
        usecols = range(len(header))
        for i in usecols:
            setattr(self, header[i], data[i])

        self.names = header

    def cleanup(self, s):
        """replace funny characters in string"""
        for char in ':/':
            s = s.replace(char, '_')
        return s


if __name__ == "__main__":
    data = loadbn("A0.dat")
    plt.plot(data.t, data.a0, 'rx')
    plt.show()

    # -- demonstrate usecols and headerstr
    data = loadbn("A0.dat", usecols=[2,3])  # only loads data.th and data.Re
    data = loadbn("A0.dat", headerstr='time aspect theta Reynolds om azero')
    plt.plot(data.time, data.azero, 'rx')
    plt.show()
