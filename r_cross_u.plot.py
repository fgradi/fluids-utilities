#!/usr/bin/env python

import numpy as np
from pdb import pm
import argparse
import loadbn
import logging
import sys
import os
#omega       = 2/2.72
#OMEGA1      = omega
#logging.warn("omega = %g hardcoded!" % omega)
#logging.warn("expecting %g" % (OMEGA1*0.3*np.pi*2))


def need_omega1(args):
    if args.omega1 is None:
        logging.error("Need OMEGA1 (-o)")
        sys.exit(-1)
    return args.omega1
  


def plot(args, case):
    import pub
    import matplotlib.pyplot as plt
    import mdl
    import os

    semtex_notation = False    
    if semtex_notation:
        Hx = 'H_x'
        Hy = 'H_y'
        Hz = 'H_z'
    else:
        Hx = 'H_z'
        Hy = 'H_x'
        Hz = 'H_y'
        
    
    OMEGA1 = need_omega1(args)
    
    fig, (ax2, ax1) = plt.subplots(nrows=2, ncols=1, sharex=False, figsize=(8,8))
    
#    map_label = {'circ':   ('b', r'$\bf\varOmega_1$'), 
#                 'rect00': ('k', r'$(\bf\varOmega_1 \times \varOmega_2) \times \varOmega_1$'),
#                 'rect09': ('r', r'$\bf\varOmega_1 \times \varOmega_2$')}
    
    T1 = 2*np.pi / OMEGA1
#    for the_file in args.infiles:
#        subcase = the_file.split('_')[1].split('.')[0]
#        try:
#            f = open(the_file, 'ra')
#            L = float(f.readline().split('=')[-1]) # read path length
    the_file = case if case.endswith('.dat') else case + '.r_cross_u.dat'
    C = loadbn.loadbn(the_file, headerstr='t Hu Hv Hw')
    if args.nondim_t:
        C.t /= T1
        #ax2.set_xlabel(r'$t/(2\pi/\varOmega_1)$')
        ax1.set_xlabel(r'$t/T_1$')
        ax2.set_xlim(0, 35)
        ax1.set_xlim(0, 35)
    else:
        ax1.set_xlabel(r'$t$')
        ax2.set_xlim(0, 650)
        ax1.set_xlim(0, 650)
        
    ax2.plot(C.t, C.Hw, 'b-', label='$%s$' % Hz)
    ax2.plot(C.t, C.Hu, 'k-', label='$%s$' % Hx)
    ax2.plot(C.t, -C.Hv, 'y-', label='$%s$' % Hy) # FIXME: find bug!
    ax2.plot(C.t, -(C.Hv**2+C.Hw**2)**0.5, 'r-', label='$-\sqrt{%s^2 + %s^2}$' % (Hy, Hz))
    ax2.plot(C.t, C.Hw*0, 'k--')
            
    if 0:
        # -- overlay on plot 1
        ax1.plot(C.t, -2.3*C.Hu, 'k-', label=r'$-2.3*u$')
        ax1.plot(C.t, 3*C.Hv, 'r-', label=r'$3*v$')

    ax2.legend(loc=(0.25,0.62), ncol=2, frameon=False)
    ax2.set_ylabel(r'$H$')
#    ax2.set_ylim(-0.025, 0.03)
        
        
    # -- plot mdl
    if not args.no_modal:
        n_modes, T, E = mdl.load_all_mdls("%s.*mdl" % case, every=10)
        if args.nondim_t: T /= T1
        ax1.plot(T, (E[1])**0.5, '-', label='1', color='r', linewidth=2)
        ax1.plot(T, (E[0])**0.5, '-', label='0', color='k', linewidth=2)
        ax1.plot(T, (E[2])**0.5, '-', label='2', color='g', linewidth=2)
      
#        ax1.plot(C.t, 2.5*(C.Hv**2+C.Hw**2)**0.5, 'r-', label='$-\sqrt{H_x^2 + H_y^2}$')

#        ax1.plot(T, (E.sum(axis=0))**0.5, '-', label='sum', color='b', linewidth=2)

        #ax1.plot(C.t, (C.Hu**2+C.Hv**2)**0.5, 'c-', label='$H_x + H_y$')

        ax1.legend(loc='lower center', frameon=False)
        ax1.set_ylabel(r'$\sqrt{E_m}$')
        #ax1.set_ylim(0, 0.1)
    #ax2.plot(T, E[2], '-', color='#aaffaa', linewidth=2)
    #ax2.plot(T, E[3], '-', color='#aaaaff', linewidth=2)
    #ax2.semilogy()
    plt.subplots_adjust(left=0.165, right=0.965, top=0.97, bottom=0.11)
    if args.interactive:
        plt.show()
    else:
        plt.savefig('%s.r_cross_u.eps' % case, transparent=False)


def main():
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("-o", '--omega1', default=None, type=float, metavar='OMEGA1', help="omega1")
    parser.add_argument("-n", '--no-modal', action="store_true", help="plot")
    parser.add_argument("-t", '--nondim-t', action="store_true", help="plot")
    parser.add_argument("-i", '--interactive', action="store_true", help="plot")
    parser.add_argument("-v", "--verbose", dest="verbose_count", action="count",
                        default=0, help="increase verbosity")
    parser.add_argument("-q", "--quiet", dest="quiet_count", action="count",
                        default=0, help="decrease verbosity")
    parser.add_argument('session', metavar='session', type=str, nargs='*',
                   help='the file')
    args = parser.parse_args()

    case = os.path.basename(os.getcwd()) if len(args.session) == 0 else args.session[0]

    plot(args, case)


if __name__ == "__main__":
    main()
#    create_rect()
#    create_circ()
#    plot()
    
