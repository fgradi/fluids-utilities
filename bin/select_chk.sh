#!/bin/bash

PROG=select_chk.sh
USAGE="$PROG [-v] t0 t1"
DESC="Move .chk files with t0 < t < t1 to selected/.  If -v is given, print time of chk files selected."
HELP="<Zero or more lines of Help>"
AUTHOR="Thomas Albrecht"

[ "$1" == "" ] || [ "$2" == "" ] || [ "$1" == "-h" ] || [ "$1" == "--help" ] && \
     echo -e "Usage: $USAGE\n$DESC\n$HELP" && exit 1

set -u
#set -x  # print commands before executing
#set -e  # exit if single command fails

verbose=
[ "$1" == "-v" ] && verbose=1 && shift

t0=$1
t1=$2

chks=`ls -1 *.chk`
mkdir -p selected/
for chk in $chks; do
    t=`head $chk | grep Time$ | cut -d" " -f1`
    result=`echo "$t < $t1 && $t > $t0" | bc -l`
    if [ "$result" == "1" ]; then
        #echo -n "$t0 < $t < $t1 "
        #echo $result
        [ "$verbose" ] && echo $t
        mv $chk selected/
    fi
done
