#!/usr/bin/env python2

import numpy as np
import sys
import matplotlib.pyplot as plt
import loadbn
from pdb import pm
import pub
import string
import DMD_io
import glob
import histogram2d as his2d

import argparse
parser = argparse.ArgumentParser(description="")
parser.add_argument('theta', metavar='theta', type=str, nargs=1,
               help='the angle (eg 1,25)')
args = parser.parse_args()


theta_s = args.theta[0]
logs = glob.glob("nomoreSFD_h2,667_t%s_R006500_k1_l1_w0,735_N05_z0064.*.log" % theta_s)
m_max = 15

pub.SetPlotRC()
plt.xlabel(r"$\omega$")
plt.ylabel(r"$\sigma$")

omega = [[] for i in xrange(m_max)]
sigma = [[] for i in xrange(m_max)]
data   = []
hist = [None for i in xrange(m_max)]
omega_fac = 1.   # account for different sampling frequencies

# -- cache data
for the_log in logs:
    data.append(DMD_io.Data_full(the_log, load_mdl=True, verbose=True))

# -- loop azim wave number.
#    9Assemble array of lists for omega[m] and sigma[m],
#    containing DMD modes of given m
ms = np.arange(m_max)
for m in ms:
    plt.clf()
    for the_data in data:
        mask = the_data.m == m
        omega[m] += list(the_data.omega[mask])
        sigma[m] += list(the_data.sigma[mask])
    bins_omega, mids_omega = his2d.mk_bins(10/omega_fac, 0.25)
    bins_sigma, mids_sigma = his2d.mk_bins(0.3, 0.03)

    the_hist = his2d.Histogram(omega[m], sigma[m], [bins_omega, bins_sigma])
    hist[m] = the_hist
    H, xedges, yedges = the_hist.H, bins_omega, bins_sigma

if 0:
    im = plt.imshow(H.transpose(), interpolation='nearest', origin='low',
                    extent=[xedges[0], xedges[-1], yedges[0], yedges[-1]])
    plt.colorbar()

    print H.shape
    print mids_omega.shape
    print mids_sigma.shape
    plt.clf()
    plt.contourf(mids_omega, mids_sigma, H.transpose())
    plt.colorbar()
    H.max()
    print logs
    print len(logs)

# -- loop logs and plot
modes_printed = False
to_print = []

for the_log, the_data in zip(logs, data):
    x0 = -10 / omega_fac
    x1 = 10 / omega_fac
    y0 = -0.3
    y1 = 0.3
    plt.clf()
    fig, (axm, axs) = plt.subplots(ncols=1, nrows=2, figsize=(5,6))

    for i, (m, to, ts) in enumerate(zip(the_data.m, the_data.omega, the_data.sigma)):
        col = pub.m_color(m)
        assert(the_data.omega[i] == to)
        assert(the_data.sigma[i] == ts)

        # -- plot modes, with marker size determined by histogram info
        #    ms code: 2-no hist, 4-fluctutating, 8-true
        ms = 2
        if m < len(hist) and m < 8:
            the_hist = hist[m]
            if the_hist is not None:
                count = the_hist.count_at(the_data.omega[i], the_data.sigma[i])[0]
                if count > 0.75*len(logs):
                    # -- print true modes
                    ms = 8
                    to_print.append(the_data.show(i))

                else:
                    ms = 4

                axs.hlines(the_hist.bins_sigma, x0, x1, '0.9')
                axs.vlines(the_hist.bins_omega, y0, y1, '0.9')


        axs.plot(the_data.omega[i], the_data.sigma[i], 'o', color=col, ms=ms)
        #axs.text(the_data.omega[i], the_data.sigma[i], '%i' % (m), color=col)

    axs.hlines([0], x0, x1, '0.6')

    axs.set_ylim(y0, y1)
    axs.set_xlim(x0, x1)
    axs.text(0, -0.2, "$%s\quad%s$" % (the_data.t0, the_data.t1))

    # -- add modal energy plot
    for m in xrange(10):
        col = pub.m_color(m)
        axm.plot(the_data.T, the_data.E[m], '-', color=col)

    axm.semilogy()
    y0 = 1e-30
    y1 = 1e-1
    axm.set_ylim(y0, y1)
    axm.vlines(the_data.t0, y0, y1, 'k')
    axm.vlines(the_data.t1, y0, y1, 'k')

    plt.savefig("%s.png" % the_log, dpi=200)

    if not modes_printed:
        print "#i  amp            omega    norm'd   sigma  ID  SNR      m triad_m triad_i"
        print string.join(to_print, "\n")
        modes_printed = True
