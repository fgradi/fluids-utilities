#!/usr/bin/env python

import numpy as np
import sys
from pdb import pm
import mdl

class Data(object):
    def __init__(self, filename, load_mdl=False, use_sparse=True, normalise=False, freq=None, verbose=False):
        def list_of_m(s):
            if s == "[]": return []
            return [int(item) for item in s[1:-1].split(',')]

        if verbose: print "reading", filename
        f = open(filename, "r")
        lines = f.readlines()
        if len(lines) == 0:
            raise ValueError("File empty")

        #i  amp            omega    norm'd   sigma  ID  SNR      m triad_m triad_i
        #0  1              2        3        4      5   6        7 8       9
        lines = [the_line for the_line in lines if not the_line.startswith("#")]

        self.i   = [int(line.split()[2])+1 for line in lines]
        if self.i == []:
            raise ValueError("No sparse data")
        self.alpha = np.array([float(line.split()[0]) for line in lines])
        self.Re    = np.array([float(line.split()[1]) for line in lines])
        self.amp   = np.array([float(line.split()[3]) for line in lines])
        self.omega = np.array([float(line.split()[4]) for line in lines])
        self.m     = np.array([int(line.split()[9]) for line in lines])
        self.sigma = np.array([float(line.split()[6]) for line in lines])
        self.SNR   = np.array([float(line.split()[8]) for line in lines])
        self.triad_m = [list_of_m(line.split()[10]) for line in lines]
        self.triad_i = [list_of_m(line.split()[11]) for line in lines]
        try:
            self.phase = np.array([float(line.split()[12]) for line in lines])
        except IndexError:
            self.phase = None

        if normalise:
            self.amp /= self.amp[0]

class Data_full(object):
    def __init__(self, filename, load_mdl=False, use_sparse=True, normalise=False, freq=None, verbose=False):
        def list_of_m(s):
            if s == "[]": return []
            return [int(item) for item in s[1:-1].split(',')]

        if verbose:
            print "reading", filename
            print "use_sparse", use_sparse
        f = open(filename, "r")
        lines = f.readlines()
        if len(lines) == 0:
            raise ValueError("File empty")

        for i, line in enumerate(lines):
            if line.startswith("# t0="):
                splitted = line.replace("=", " ").split()
                self.t0 = float(splitted[2])
                self.t1 = float(splitted[6])

            if use_sparse:
                if line.startswith("# saving sparse"):
                    lines = lines[i+5:]
                    break
            else:
                if line.startswith("#i"):
                    i0 = i+1
                if line.startswith("# saving chks"):
                    lines = lines[i0:i]
                    break

        #i  amp            omega    norm'd   sigma  ID  SNR      m triad_m triad_i
        #0  1              2        3        4      5   6        7 8       9
        lines = [the_line for the_line in lines if not the_line.startswith("#")]

        self.i   = [int(line.split()[0])+1 for line in lines]
        if self.i == []:
            raise ValueError("No sparse data")
        self.amp   = np.array([float(line.split()[1]) for line in lines])
        self.omega = np.array([float(line.split()[2]) for line in lines])
        # -- [:-1] removes trailing x
        self.normd = np.array([float(line.split()[3][:-1]) for line in lines])
        self.m     = np.array([int(line.split()[7]) for line in lines])
        self.sigma = np.array([float(line.split()[4]) for line in lines])
        self.SNR   = np.array([float(line.split()[6]) for line in lines])
        self.triad_m = [list_of_m(line.split()[8]) for line in lines]
        self.triad_i = [list_of_m(line.split()[9]) for line in lines]
        try:
            self.phase = np.array([float(line.split()[10]) for line in lines])
        except IndexError:
            self.phase = None

        if normalise:
            self.amp /= self.amp[0]

        if load_mdl:
            mdl_file = filename[:filename.find("_z0064")+6]+".from_chk.mdl"
            self.n_modes, self.T, self.E = mdl.load_mdl(mdl_file)

    def show(self, i):
        s = "%02i %13.6e % 8.4f %7.3fx %8.4f :%s" % (i, abs(self.amp[i]),
            self.omega[i], self.normd[i], self.sigma[i], '_')

        s += " %7.1e %2i" % (self.SNR[i], self.m[i])
        s += " []"
        s += " []"
        s += " %7.3f" % self.phase[i]
        s += " %02i" % i
        return s
