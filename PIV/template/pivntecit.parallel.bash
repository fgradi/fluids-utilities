#!/bin/bash

# PIV and convert to tec, 
# save to run multiple instances on a single dir
# run in dir containing
# - $case*.tif
# - $case.par
# crop images before PIV, need roi.py 

# set -x
echo "$0 running"

dist=500 # between two frames
step=100  # process every n'th frame
nice='nice -n 19'
mask="mask_crop.tif"
piv3cfg=piv3c.cfg
dewarp=
dpiv=/mnt/hgfs/albrecht/monash/derek/range/bin/pivview-3.0.9/dpiv
ncdf2tec=/mnt/hgfs/albrecht/monash/derek/range/bin/pivview-3.0.9/ncdf2tec

# get stem from first tif
tmp=`ls -1 *001.tif | head -n1`
stem=${tmp%00000001.tif}
echo "stem <$stem>"

parfile=`ls -1 *.par | head -n1`
n=`ls -1 | grep "${stem}[0-9]\{8\}\.tif" | tail -n1 | sed "s/${stem}//" | sed 's/\.tif//'`
echo "n=$n"
last=`echo ${n} - ${dist} | bc`	# -- use bc because bash would complain on leading zeros
mkdir -p nc/ tif/ dat/ tmp/1/ tmp/2/ lock/
echo "last <${last}>"

# -- read crop info and crop mask
. roi.py
(( dx = x1 - x0 ))
(( dy = y1 - y0 ))
convert mask.tif -crop ${dx}x${dy}+${x0}+${y0} ${mask}


for (( i=${step} ; ${i} <= ${last} ; i+=${step} ))
do
    nfirst=`printf "%08d" $i`
    nsecond=`printf "%08d" $(( ${i} + ${dist} ))`
    #echo ${nfirst} ${nsecond}
    first="${stem}${nfirst}.tif"
    second="${stem}${nsecond}.tif"
    
    ls -l  "$first" "$second"

    # -- check for first file, save second, lock first
    [ ! -r "${first}" ] && continue
    cp "$second" tmp/2/	# does this fix potential race condition?
    lock="lock/${first}"
    if ! mkdir "$lock" >& /dev/null; then
	continue
    fi
    
    # -- dpiv
    echo "${first}" "${second}"
    mv "$first" tmp/1/  ## FIXME!
    first="tmp/1/$first"
    tsecond="tmp/2/$second"
    pivout="${stem}${nfirst}.nc"

    # -- de-warp
    if [ $dewarp ]; then
        defirst="tmp/${nfirst}-dewarped-1st"
        desecond="tmp/${nsecond}-dewarped-2nd"
        $nice mapimage -info -a ${piv3cfg} "${first}"  "${defirst}"
        $nice mapimage -info -a ${piv3cfg} "${tsecond}" "${desecond}"
    else
        defirst="${first}"
        desecond="${tsecond}"
    fi

    
    # -- crop images
    convert -quiet ${defirst} -crop ${dx}x${dy}+${x0}+${y0} 1.tif
    convert -quiet ${desecond} -crop ${dx}x${dy}+${x0}+${y0} 2.tif

    $nice $dpiv -mask${mask} -o${pivout}  ${parfile} 1.tif 2.tif
    rm 1.tif 2.tif

    rm "$tsecond"
    [ $dewarp ] && rm "${defirst}" "${desecond}"
    
    
    # -- convert data to tecplot format
    tecout="dat/${stem}${nfirst}.dat"
    $nice $ncdf2tec -asc -piv ${pivout} ${tecout}
    
    # -- clean up
    #$nice bzip2 ${pivout}
#     mv $pivout nc/
    rm $pivout
    #$nice bzip2 "${first}"	# -- zip skips files with more than one link
    mv ${first} tif/
    rmdir "$lock"
done
# exit
#$nice bzip2 "${second}"; mv "${second}".bz2 tif/
# -- clean up skipped files
#$nice bzip2 tmp/1/*.tif
mv tmp/1/*.tif.bz2 tif/
rmdir tmp/1 tmp/2 tmp nc lock
