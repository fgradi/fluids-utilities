#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A library to access PIV data stored in different formats.
Currently implemented:
- (gzipped) Tecplot
- NetCDF
"""

# Uniread is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# Uniread is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# Copyright (C) 2009-2017 Tom Weier and Thomas Albrecht


# -- unified PIV data reader
import numpy as np
import sys
import gzip
import bz2
import re
from importruntime import importRuntime

class uniread:
    """unified PIV data reader: determine file type automagically,
       read data (grid, velocity, Re-stress).
       Currently implemented formats: (gzipped) Tecplot, NetCDF.
       If config file name is given, nondimensionalize the data.
       Dimension nx, ny and field variables can be overridden
       (so far only implemented for Tecplot reader).
    """
    def __init__(self, filename, filetype=None, config=None,
                 nx=None, ny=None, variables=None, delimiter=None,
                 transpose=False):
        if not filetype:
            filetype = self.get_filetype(filename)
        self.x_min    = 0
        self.x_max    = 0
        self.y_min    = 0
        self.y_max    = 0
        self.dx       = 0
        self.dy       = 0
        self.u        = []
        self.v        = []
        self.flag     = []
        self.x        = []
        self.y        = []

        # "vectorization" via universal functions
        self.get_flag_from_netcdf = np.frompyfunc(get_flag_scalar_from_netcdf,1,1)
        self.get_flag_from_tecplot = np.frompyfunc(get_flag_scalar_from_tecplot,1,1)

        if filetype == 'NETCDF':
            self.read_netcdf(filename)
        elif filetype == 'TECPLOT' or filetype == "funnyTECPLOT":
            self.read_tecplot(filename, nx, ny, variables, delimiter, transpose)
        else:
            raise TypeError ("Unknown filetype, giving up")

        if config: self.nondim(config)

    def uniopen(self, fname, mode='r'):
        """open a file. Try gzip/bzip2/direct access."""
        f = gzip.open(fname, mode)
        try:
            f.readline()
        except IOError:
            pass
        else:
            f.seek(0)
            return f

        f = bz2.BZ2File(fname, mode)
        try:
            f.readline()
        except IOError:
            pass
        else:
            f.seek(0)
            return f

        f = open(fname, mode)
        return f

    def get_filetype(self, filename):
        """try to determine file type"""
        ftype = 'OTHER'
        f = self.uniopen(filename, 'rb')
        # -- NetCDF
        magic = f.read(3)
        if magic == 'CDF':
            f.close()
            return 'NETCDF'
        # -- 'standard' Tecplot
        f.seek(0)
        for line in f:
            if (line[0:4] == 'ZONE'):
                f.close()
                return 'TECPLOT'
        # -- funny Tecplot format
        f.seek(0)
        s = f.readline()
        if 'ZONE' in s:
            return 'funnyTECPLOT'
            f.close()
        f.close()
        return ftype

    def read_netcdf(self, filename) :
        from pupynere import NetCDFFile
        file          = NetCDFFile(filename, 'r')
        flags         = file.variables['piv_flags'][0,:,:]
        piv_data      = file.variables['piv_data']
        file_ori_name = file.long_name
        self.u = piv_data[0,:,:,0]
        self.v = piv_data[0,:,:,1]
        self.x_min = piv_data.coord_min[0]
        self.x_max = piv_data.coord_max[0]
        self.y_min = piv_data.coord_min[1]
        self.y_max = piv_data.coord_max[1]
        self.dx = file.ev_IS_grid_distance_x
        self.dy = file.ev_IS_grid_distance_y
        self.ny, self.nx = self.u.shape
        self.flag = np.array(self.get_flag_from_netcdf(flags), copy = False, subok = True, dtype=np.int8)
        self.flag.shape = (self.ny,self.nx)
        self.x = np.linspace(self.x_min,self.x_max,self.nx)
        self.y = np.linspace(self.y_min,self.y_max,self.ny)
        self.X, self.Y = np.meshgrid(self.x,self.y)
        self.X = self.X.flatten()
        self.Y = self.Y.flatten()
        self.sx = self.x_max - self.x_min
        self.sy = self.y_max - self.y_min


    def have(self, v):
        """check for given variable. Return True if found."""
        return (v in self.variables)

    def read_tecplot(self, filename, nx = None, ny = None, variables = None,\
                     delimiter = None, transpose = False):
        def _index(v):
            """return index of given variable"""
            i = 0
            for V in self.variables:
                if V == v:
                    return i
                i += 1
            return -1

        def _fix_shape(a):
            """fix shape, take transpose flag into account"""
            if transpose:
                return a.reshape(self.nx, self.ny).transpose()
            else:
                return a.reshape(self.ny, self.nx)

        f = self.uniopen(filename, 'r')
        headerlines = 0
        self.variables = []

        for line in f:
            line = line.lstrip()
            headerlines += 1
            if len(line) == 0: continue
            first_char = line[0]
            if first_char.isdigit() or first_char in '-+': break
            if first_char == '#': continue
            if line.startswith('VARIABLES'):
                # -- read variables from tecplot header
                # FIXME: this skips last variable
                # if variables are not enclosed in "
                self.variables = re.split('[",= ]*', line)[1:-1]
            elif line.startswith('ZONE'):
                m = re.search('I=[0-9]+', line)
                if m:
                    self.nx = int(m.group().split('=')[1])
                else:
                    print "ERROR: can't find I index in header"
                    # FIXME: error handling, exit
                m = re.search('J=[0-9]+', line)
                if m:
                    self.ny = int(m.group().split('=')[1])
                else:
                    print "ERROR: can't find J index in header"

        headerlines -= 1

        if transpose:
	    self.nx, self.ny = self.ny, self.nx

        # -- all can be overridden by cmd line
        if nx is not None: self.nx = nx
        if ny is not None: self.ny = ny
        if variables is not None: self.variables = variables

        f.seek(0)
        # -- Try both default (whitespace) and comma delimiters
        try:
            T = np.loadtxt(f,skiprows=headerlines, delimiter=delimiter)
        except ValueError:
            f.seek(0)
            T = np.loadtxt(f,skiprows=headerlines, delimiter=',')

        # -- map external variables to internal ones
        #    e.g. accept c|x, r|y for coordinates and dx|u, dy|v for velocities
        #    dict {external: internal}
        #    if external not in dict, use it right away
        var_map = {'c':'X', 'x':'X', 'r':'Y', 'y':'Y', 'z':'Z',
                   'dx':'u', 'vx':'u', 'dy':'v', 'vy':'v',
                   'uup':'uu', 'uvp':'uv', 'uwp':'uw', 'vvp':'vv', 'vwp':'vw', 'wwp':'ww',
                   'dxdx':'uu', 'dxdy':'uv', 'dydy':'vv' }
#        print var_map
        translated = []
        for external in self.variables:
            if external in var_map:
                internal = var_map[external]
            else:
                internal = external
            vars(self)[internal] = _fix_shape(T[:, _index(external)])
            translated.append(internal)
        self.original_variables = self.variables
        self.variables = translated

        self.X = self.X.flatten()
        self.Y = self.Y.flatten()
        self.update_geometry()

        if self.have('flag'):
            self.flag = np.array(self.get_flag_from_tecplot(T[:, _index('flag')]), copy = False, subok = True, dtype=np.int8)
            self.flag = _fix_shape(self.flag)

        # -- optional fields: Re-stresses
        self.rst = False
        if self.have('uu') or self.have('dxdx'):
            self.rst = True


    def write_tecplot_header(self, f=sys.stdout, title='', variables=[]):
        """write a simple tecplot header using current dimensions"""
        f.write('TITLE = "%s"\n' % title)
        f.write('VARIABLES = %s\n' % str(variables).strip("[]").replace("'", '"'))
        f.write('ZONE I=%i, J=%i, F=Point, T="%s"\n' % (self.nx,self.ny, title))

    def write_tecplot_data(self, fname, title='', variables=[], showN = False,
                           showZ = False, showCoords = True):
        sys.stderr.write("Deprecated call to 'uniread.write_tecplot_data'. Use 'uniread.write_tecplot'.")
        self.write_tecplot(fname, title, variables, showN, showZ, showCoords)

    def write_tecplot(self, fname, title='', variables=[], showN = False,
                      showZ = False, showCoords = True):
        """Write tecplot data. Supports plain/gzip/bz2, based on file extension.

        Example:
           write_tecplot_data('field.dat.bz2', variables=['u','v'])
           write self.u and self.v to file, bzip'd. By default, includes
           X,Y coordinates.
        """

        if fname.split('.')[-1] == 'gz':
            f = gzip.open(fname, 'w')
        elif fname.split('.')[-1] == 'bz2':
            f = bz2.BZ2File(fname, 'w')
        else:
            f = open(fname, 'w')

        if showCoords:
            variables.insert(0, 'X')
            variables.insert(1, 'Y')
            if showZ: variables.insert(2, 'Z')
        if showN:
            variables.insert(0, 'n')
            self.n = np.arange(self.nx * self.ny)

        self.write_tecplot_header(f, title, variables)

        fields = []
        for v in variables:
            fields.append(vars(self)[v].flatten())

        for i in range(0, len(self)):
            if i and i % self.nx == 0: f.write('\n')
            for nf in range(len(variables)):
                f.write("%g\t" % fields[nf][i])
            f.write('\n')

        if showN: self.n = None
        f.close()

    def update_geometry(self):
        """update x_min, dx & friends. Call if X/Y has changed."""

        self.x_min    = self.X[ 0]
        self.x_max    = self.X[-1]
        self.y_min    = self.Y[ 0]
        self.y_max    = self.Y[-1]

        self.x        = np.linspace(self.x_min,self.x_max,self.nx)
        self.y        = np.linspace(self.y_min,self.y_max,self.ny)

        self.dx       = self.x[1] - self.x[0]
        self.dy       = self.y[1] - self.y[0]

        self.sx       = self.x_max - self.x_min
        self.sy       = self.y_max - self.y_min

    def nondim(self, config):
        """nondimensionalize data using reference values given in config file/object"""

        if isinstance(config, str):
            self.config = importRuntime(config)
        else: self.config = config

	   # -- scale data.
        #    PPM == pixel per meter
        PPM = 0.5 * (self.config.PX_PER_METER + self.config.PY_PER_METER)

        self.X = (self.X - self.config.PX_ORIGIN)/(self.config.PX_PER_METER * self.config.LREF)
        self.Y = (self.Y - self.config.PY_ORIGIN)/(self.config.PY_PER_METER * self.config.LREF)
        self.update_geometry()
        self.u *= 1./(self.config.PX_PER_METER * self.config.DT * self.config.UREF)
        self.v *= 1./(self.config.PY_PER_METER * self.config.DT * self.config.UREF)

        if self.have('uu'):
            self.uu *= 1./(PPM * self.config.DT * self.config.UREF)**2.
        if self.have('uv'):
            self.uv *= 1./(PPM * self.config.DT * self.config.UREF)**2.
        if self.have('vv'):
            self.vv *= 1./(PPM * self.config.DT * self.config.UREF)**2.

        return self

    def nondim_from_mm(self, fname):
        """nondimensionalize data which is already in m/s etc. using reference
           values given in conf file"""

        self.config = importRuntime(fname)
        # -- scale data.
        #    PPM == pixel per meter
        PPM = 0.5 * (self.config.PX_PER_METER + self.config.PY_PER_METER)

        self.X = self.X / self.config.LREF / 1000.
        self.Y = self.Y / self.config.LREF / 1000.
        self.update_geometry()
        self.u *= 1./(self.config.UREF) / 1000.
        self.v *= 1./(self.config.UREF) / 1000.

        if self.have('uu'):
            sys.stderr.write("nondim_from_mm: conversion untested, probably wrong!\n")
            self.uu *= 1./(self.config.UREF)**2.
        if self.have('uv'):
            self.uv *= 1./(self.config.UREF)**2.
        if self.have('vv'):
            self.vv *= 1./(self.config.UREF)**2.

        return self

    def __len__(self):
        """return flattened length"""
        return self.nx * self.ny

    def flipX(self):
        """flip data in x"""
        self.u = -self.u[:,::-1]
        self.v =  self.v[:,::-1]

        # -- optional fields
        if self.have('flag'):
            self.flag = self.flag[:,::-1]
        if self.have('uu'):
            self.uu = self.uu[:,::-1]
        if self.have('vv'):
            self.vv = self.vv[:,::-1]
        if self.have('uv'):
            self.uv = -self.uv[:,::-1]
        if self.have('p'):
            self.p = self.p[:,::-1]

    def flipY(self):
        """flip data in y"""
        self.u =  self.u[::-1,:]
        self.v = -self.v[::-1,:]

        # -- optional fields
        if self.have('flag'):
            self.flag = self.flag[::-1,:]
        if self.have('uu'):
            self.uu = self.uu[::-1,:]
        if self.have('vv'):
            self.vv = self.vv[::-1,:]
        if self.have('uv'):
            self.uv = -self.uv[::-1,:]
        if self.have('p'):
            self.p = self.p[::-1,:]

    def pos2idx(self, x, y):
        """accept position, return nearest index"""
        i = int(round((x - self.x_min) / self.dx))
        j = int(round((y - self.y_min) / self.dy))
        return i, j


def get_flag_scalar_from_tecplot(tec_flag) :
    """Transform tecplot file flags to named flags

    pivview status flags in netcdf/tecplot files

    status flags partly taken from pivviews pivdefs.h, partly observed (0x21)
    relation to tecplot-file-flags are guessed

    netcdf-flag   tecplot-flag            meaning

    0,0x10        DISABLED     (0)   disabled
    0x02,0x04     NORESULT     (1)   no result available
    0x08          OUTLIER      (2)   outlier
    0x01          VALIDDATA    (3)   ValidData
    0x40          OTHERPEAK    (4)   OtherPeak
    0x20,0x21     INTERPOLATED (5)   Interpolated
    """
    if tec_flag >= 0 and tec_flag <= 6:
        return tec_flag
    else:
        return Flags.OTHERPEAK

class Flags:
    """Enum type for flags"""
    DISABLED, NORESULT, OUTLIER, VALIDDATA, OTHERPEAK, INTERPOLATED, FILL = range(7)

def get_flag_scalar_from_netcdf(netcdf_flag) :
    """Transforms netcdf_flags to named flags

    pivview status flags in netcdf files

    status flags partly taken from pivviews pivdefs.h, partly observed (0x21)
    relation to tecplot-file-flags are guessed

    netcdf-flag   tecplot-flag            meaning

    0,0x10        DISABLED     (0)   disabled
    0x02,0x04     NORESULT     (1)   no result available
    0x08          OUTLIER      (2)   outlier
    0x01          VALIDDATA    (3)   ValidData
    0x40          OTHERPEAK    (4)   OtherPeak
    0x20,0x21     INTERPOLATED (5)   Interpolated
    """
    if ( ( netcdf_flag == 0 ) or ( netcdf_flag == 0x10 ) ):
        return Flags.DISABLED
    elif ( ( netcdf_flag == 0x02 ) or ( netcdf_flag == 0x04 ) ):
        return Flags.NORESULT
    elif ( netcdf_flag == 0x08 ):
        return Flags.OUTLIER
    elif ( netcdf_flag == 0x01 ):
        return Flags.VALIDDATA
    elif ( netcdf_flag == 0x40 ):
        return Flags.OTHERPEAK
    elif ( ( netcdf_flag == 0x20 ) or ( netcdf_flag == 0x21 ) ):
        return Flags.INTERPOLATED
    else :
        return Flags.OTHERPEAK


def main():
    T = uniread("/home/albrecht/fzd/platte_kleiner_kanal/paper-pivforce-post/PIV/piv8x8/a.dat")
    #M = uniread('../test/piv-data/ring_f30_s150_u04_00__0898.dat')
    print M.u[:,20]
    print M.v[:,20]

if __name__ == "__main__":
    main()
